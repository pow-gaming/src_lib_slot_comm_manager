export type CurrencyConfig = Object & {
    mask: string;
    config: any & {
        delimiters: Object & {
            thousands: string,
            decimal: string
        },
        currency: Object & {
            symbol: string
        }
    };
}