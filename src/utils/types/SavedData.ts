import Dictionary from "../Dictionary";

/** @internal */
export class SavedData {
    private dictionary: Dictionary<string, any> = new Dictionary<string, any>();

    constructor() {

    }

    add(name:string, data:any){
        this.dictionary.setValue(name, data);
    }

    get(name:string):any {
        return this.dictionary.getValue(name);
    }
}