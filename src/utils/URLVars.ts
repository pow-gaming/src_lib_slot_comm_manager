/** @internal */
export class URLVarsClass {

  private regExp: RegExp = /[?#&\/)]+([^=?&#\/]+)=([^&]*)/gi;
  private urlVars: any;

  constructor() {
    this.urlVars = this.getUrlVars();
  }

  getUrlVars() {
    let vars: any = {};
    let url: string = window.location.href;
    url.replace(this.regExp, function(m: string, key: string, value: string) {
      vars[key] = value;
      return null;
    });

    return vars;
  }

  getQueryVariable(name: string) {
    return this.urlVars[name];
  }

}
/** @internal */
export var URLVars: URLVarsClass = new URLVarsClass();
