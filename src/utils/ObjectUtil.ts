/** @internal */
export class ObjectUtil {

    private static DELIMITATOR:string = ".";
    private static UNDEFINED:string = "undefined";
    private static EMPTY_STRING:string = "";
    private static REG_EXP:any = /[\.\[\]\"\']{1,2}/;


    static getObjectByMultiplePaths<T extends Object>(target:T, paths:string[]):T {
        var object:any;
        for (var path of paths) {
            object = ObjectUtil.getObjectByPath<T>(target, path);
            if(object != null) {
                return object;
            }
        }
        return null;
    }


    static getObjectByPath<T extends Object>(target:T, aPath:string):any {
        if (typeof target === ObjectUtil.UNDEFINED || target === null) return;
        var path = aPath.split(ObjectUtil.REG_EXP);
        for (var i = 0, l = path.length; i < l; i++) {
            if (path[i] === ObjectUtil.EMPTY_STRING) continue;
            target = (<any>target)[path[i]];
            if (typeof target === ObjectUtil.UNDEFINED || target === null) return;
        }
        return target;
    }

    static existObjectByPath<T extends Object>(target:T, path:string):boolean {
        return ObjectUtil.getObjectByPath(target, path) != null;
    }

    static getObjectSequence<T extends Object>(target:any, prefix:string, startingIndex:number = 0, ammount:number = Number.POSITIVE_INFINITY):Array<T> {
        var objects:Array<T> = [];

        var i = startingIndex;
        while(target[prefix + i] != null && i - startingIndex < ammount) {
            objects.push(target[prefix + i]);
            i++;
        }

        return objects;
    }
    
    static findProp(obj:Object, findProp:string) {
        var result:any[] = [];
        var finalResult = ObjectUtil.hasProp(obj, findProp, result);
        return finalResult.length > 0 ? finalResult[0].value : null;

    }
    
    static findByMethod(obj:Object, methodName:string) {
        var result:any[] = [];
        result = ObjectUtil.hasProp(obj, "method", result);
        if(result != null){
            for(var i = 0; i < result.length; i++){
                if(result[i].value == methodName){
                    return result[i].obj;
                }
            }
        }
        return null;
    }

    static hasProp(obj:Object, findProp:string, result:any[]):any[] {
        for (const prop in obj) {
            const value = (<any>obj)[prop];
            if (prop == findProp && value != null) {
                result.push({obj: obj, prop: prop, value: value});
            }
            if (typeof value === 'object') {
                ObjectUtil.hasProp(value, findProp, result);
            }
        }
        return result;
    }
}