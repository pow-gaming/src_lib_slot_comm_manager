import { Signal } from 'signals';
import { ICommand } from '@vibragaming/command';

/** @internal */
export interface ITask extends ICommand {
    id: string;
    onProgress: Signal;
    onCanceled: Signal;

    progress: number;
    weight: number;

    canCancel: () => boolean;
    cancel: () => void;
}


