import { ITask } from './ITask';

/** @internal */
export interface ITasks extends ITask {
    addTask(task:ITask): void;
}
