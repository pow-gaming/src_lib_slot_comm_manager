import { Signal } from 'signals';
import { ITask } from './ITask';
import { ICommand } from '@vibragaming/command';

/** @internal */
export class AbstractTask implements ITask, ICommand {

    private static PROGRESS_COMPLETE: number = 1;
    private _progress: number = 0;

    onComplete: Signal = new Signal();
    onError: Signal = new Signal();

    onProgress: Signal = new Signal();
    onCancelRequested: Signal = new Signal();
    onCanceled: Signal = new Signal();
    protected _canceled: boolean = false;

    constructor(private _id: string = "", private _weight: number = 1) {
    }

    execute(params: any = null): void {
        this.resetCanceled();
    }

    protected resetCanceled(): void {
        this._canceled = false;
    }

    canCancel(): boolean {
        return !this._canceled;
    }

    cancel(): void {
        this.sendCanceled();
    }

    get id(): string {
        return this._id;
    }

    get progress(): number {
        return this._progress;
    }

    get weight(): number {
        return this._weight;
    }

    //value between 0-1
    protected sendProgress(): void {
        this.onProgress.dispatch(this._progress);
    }

    protected sendCanceled(...args): void {
        this._canceled = true;
        this.onCanceled.dispatch.apply(this, args);
    }

    protected sendError(...args): void {
        this.onError.dispatch.apply(this, args);
    }

    protected sendComplete(): void {
        this._progress = AbstractTask.PROGRESS_COMPLETE;
        this.sendProgress();

        this.onComplete.dispatch();
    }


    destroy(): void {
        if (this.onProgress != null) {
            this.onProgress.removeAll()
        }
        if (this.onCancelRequested != null) {
            this.onCancelRequested.removeAll()
        }
        if (this.onCanceled != null) {
            this.onCanceled.removeAll()
        }

        if (this.onComplete != null) {
            this.onComplete.removeAll();
        }
        if (this.onError != null) {
            this.onError.removeAll();
        }

        this.onComplete = null;
    }
}
