import { AbstractTask } from './AbstractTask';
import { ITasks } from './ITasks';
import { ITask } from './ITask';

/** @internal */
export class SequenceTask extends AbstractTask implements ITasks {

    private _tasks:ITask[] = new Array<ITask>();


    private _pendingTasks:ITask[] = new Array<ITask>();
    private _executedTasks:ITask[] = new Array<ITask>();

    private _currentTask:ITask;


    constructor(tasks:ITask[] = null, id:string = "", weight:number = 1) {
        super(id, weight);

        if (tasks != null) {
            for (var task of tasks) {
                this.addTask(task);
            }
        }
    }

    addTask(task:ITask):void {
        this._tasks.push(task);
    }

    get progress():number {
        //calculate progress
        var totalProgress:number = 0;
        var currentProgress:number = 0;

        for (var task of this._pendingTasks) {
            currentProgress += task.progress * task.weight;
            totalProgress += task.weight;
        }

        for (var task of this._executedTasks) {
            currentProgress += task.progress * task.weight;
            totalProgress += task.weight;
        }
        return currentProgress / totalProgress;
    }

    cancel():void {
        if (this._currentTask != null && !this._canceled && this._pendingTasks != null) {

            this._currentTask.cancel();
            this.removeTaskListeners(this._currentTask);
            this._currentTask = null;
            this._pendingTasks = null;
            this._executedTasks = null;
            super.cancel();
        }
    }

    canCancel():boolean {
        return this._currentTask == null || this._currentTask.canCancel();
    }

    protected sendComplete():void {
        this.removeTaskListeners(this._currentTask);
        this._currentTask = null;
        super.sendComplete();
    }

    public execute(params:any = null):void {
        this._canceled = false;
        this._pendingTasks = this._tasks.concat();
        this._executedTasks = [];
        if(this._tasks.length == 0 ) {
            this.sendComplete();
        } else {
            this.executeNextTask(params);
        }
    }


    private executeNextTask(params:any = null):void {

        if (this._pendingTasks != null && this._pendingTasks.length <= 0) return;

        this.removeTaskListeners(this._currentTask);

        this._currentTask = this._pendingTasks.splice(0, 1)[0];
        this._executedTasks.push(this._currentTask);

        this._currentTask.onComplete.addOnce((this._pendingTasks.length > 0) ? this.executeNextTask.bind(this, params) : this.sendComplete, this);
        this._currentTask.onError.addOnce(this.onError.dispatch, this.onError);
        this._currentTask.onCanceled.addOnce(this.onCanceled.dispatch, this.onCanceled);
        this._currentTask.onProgress.addOnce(this.sendProgress, this);
        this._currentTask.execute(params);
    }


    private removeTaskListeners(task:ITask):void {
        if (task != null) {
            task.onComplete.removeAll();
            task.onError.removeAll();
            task.onCanceled.removeAll();
            task.onProgress.removeAll();
        }
    }

    private destroyEachItem(task:ITask):void {
        task.destroy();
    }

    public destroy():void {
        this._executedTasks.forEach(this.destroyEachItem.bind(this));
        this._executedTasks = null;

        this._pendingTasks.forEach(this.destroyEachItem.bind(this));
        this._pendingTasks = null;

        this._tasks.forEach(this.destroyEachItem.bind(this));
        this._tasks = null;

        super.destroy();
    }
}


