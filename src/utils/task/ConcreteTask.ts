import { AbstractTask } from "./AbstractTask";

/** @internal */
export class ConcreteTask extends AbstractTask {
    constructor(protected params:any = null){
        super();
    }
}