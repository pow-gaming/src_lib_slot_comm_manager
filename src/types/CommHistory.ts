import { CommSpinResponse } from "./CommSpinResponse";
import { CommBonusResponse } from "./CommBonusResponse";

export type CommHistory = {
    history: Array<CommSpinResponse | CommBonusResponse>
}