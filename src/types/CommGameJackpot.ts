export type CommGameJackpot = {
    active: boolean,
    awarded: boolean,
    isDeferred: boolean,
    eligibleUsers: number | null,
    contribution: number | number[],
    currency: string,
    details: string,
    id: number,
    name: string,
    isProgressive: boolean
    prize: number,
    seeding: number | number[]
}