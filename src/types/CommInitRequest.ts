export type CommInitRequest = {
    version: string,
    lobby: any,
    gameRules: any,
    useCustomAboutScreen: boolean,
    onJackpotUpdate: Function,
    onBalanceUpdate: Function,
    onSoundUpdate: Function,
    onInfoUpdate: Function,
    onErrorHandled: Function,
    onActionRequested: Function,
    extraParams: any
};