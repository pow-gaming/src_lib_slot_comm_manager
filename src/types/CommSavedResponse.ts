export type CommSavedResponse = {
    name: string,
    data: any
}