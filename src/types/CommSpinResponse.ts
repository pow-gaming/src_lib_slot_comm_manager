import { CommGameFreePlays } from "./CommGameFreePlays";
import { CommResponseStates } from "./CommResponseStates";

export type CommSpinWinningsLine = {
    creditsWon: number,
    symbol: number | string,
    line: number | string,
    positions: number[],
    count: number,
    multiplier: number,
    extraData: any
}

export type CommSpinWinnings = {
    creditsWon: number,
    lines: CommSpinWinningsLine[]
}

export type CommSpinResponse = {
    creditsWon: number,
    bonus: any[],
    freePlays: CommGameFreePlays,
    reel: number[] | number[][] | string[] | string[][],
    reelKey: string,
    winnings: CommSpinWinnings,
    type: CommResponseStates,
    getBonus: (name:string) => any,
    getProp: (name:string) => any
}