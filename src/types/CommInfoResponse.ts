export type CommInfoResponse = {
    controlPanel: boolean,
    about: boolean,
    paytable: boolean,
    help: boolean
};