export type CommSoundResponse = {
    general: boolean,
    music: boolean,
    sfx: boolean
};