import { CommGameFreePlays } from "./CommGameFreePlays";
import { CommResponseStates } from "./CommResponseStates";
import { CommGameJackpot } from "./CommGameJackpot";

export type CommInitResponse = {
    balance: {
        balance: number,
        freeBalance: number
    },
    autoplay: {
        value: number,
        list: number[]
    },
    history: any,
    bet: {
        value: number,
        list: number[]
    },
    rtp: {
        game: number,
        jackpot: number,
        upper: number
    },
    freePlays: CommGameFreePlays,
    jackpots: CommGameJackpot[],
    lines: number[][],
    lobby: Function,
    gameRules: Function,
    keyboardEnabled: boolean,
    logoEnabled: boolean,
    customConfig: {
        customAssets: string,
        customIndex: number
    },
    extraParams: any,
    type: CommResponseStates
};
