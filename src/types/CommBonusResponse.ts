import { CommResponseStates } from "./CommResponseStates";

export type CommBonusResponse = {
    bonus: any[],
    type: CommResponseStates,
    getBonus: (name:string) => any
}