export type CommGameFreePlays = {
    lines: number,
    bet: number,
    totalWin: number,
    playsDone: number,
    playsLeft: number,
    playsTotal: number
};