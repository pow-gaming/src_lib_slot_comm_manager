export enum CommGameStates {
    INITIALIZE,
    IDLE,
    OPEN,
    INPLAY,
    SETTLE,
    CLOSE,
    RESULTS,
    READY
}