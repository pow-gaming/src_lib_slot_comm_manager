import { CommActionType } from "./CommActionType";

export type CommActionData = {
    action: CommActionType,
    confirmation: (success:boolean) => void;
}
