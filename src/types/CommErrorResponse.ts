export type CommErrorResponse = {
    code: number,
    userConfirmed: boolean,
    userCancelled: boolean,
    isCritical: boolean,
    message: string
};