import { CommResponseStates } from "./CommResponseStates";

export type CommGambleResponse = {
    bonus: any[],
    type: CommResponseStates,
    getBonus: (name:string) => any
}