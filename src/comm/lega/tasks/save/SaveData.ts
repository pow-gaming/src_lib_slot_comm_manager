import { ConcreteTask } from "../../../../utils/task/ConcreteTask";

declare var leanderGMApi:any;

/** @internal */
export class SaveData extends ConcreteTask {
    private serverRequestCompletedHandler:number;
    private serverRequestFailedHandler:number;

    execute(params:any = null): void {
        this.serverRequestCompletedHandler = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_COMPLETED,
            this.executeActionOnSuccess.bind(this)
        );

        this.serverRequestFailedHandler = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_FAILED,
            this.executeActionOnFail.bind(this)
        );

        leanderGMApi.sendRequest({
            "method": "saveData",
            "params":
            {
                "data": this.params.data,
                "name": this.params.name,
                "sessionId": leanderGMApi.getSessionId()
            }
        });
    }
    
    private executeActionOnSuccess(response: any): void {
        this.sendComplete();
        this.removeEventsSuscribed();
    }

    private executeActionOnFail(response: any): void {
        this.sendError();
        this.removeEventsSuscribed();
    }
    
    private removeEventsSuscribed(): void {
        leanderGMApi.unsubscribeFromEvent(this.serverRequestCompletedHandler);
        this.serverRequestCompletedHandler = null;

        leanderGMApi.unsubscribeFromEvent(this.serverRequestFailedHandler);
        this.serverRequestFailedHandler = null;
    }
}