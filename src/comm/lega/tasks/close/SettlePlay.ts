import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ObjectUtil } from "../../../../utils/ObjectUtil";
import { LegaModel } from "../../LegaModel";
import { LegaErrors } from "../../LegaErrors";

declare var leanderGMApi:any;

/** @internal */
export class SettlePlay extends ConcreteTask {
    private serverRequestCompleted: number;
    private serverRequestFailed: number;

    execute(params:any = null): void {        
        let sessionId = leanderGMApi.getSessionId();

        let requestData: Array<any> = [
            { 
                "method": "settlePlay", 
                "params": { "sessionId": sessionId } 
            }
        ];

        this.serverRequestCompleted = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_COMPLETED,
            this.executeActionOnSuccess.bind(this)
        );

        this.serverRequestFailed = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_FAILED,
            this.executeActionOnFail.bind(this, LegaErrors.NETWORK_ERROR)
        );

        leanderGMApi.sendRequest(requestData);
    }
    
    private executeActionOnSuccess(response: any): void {
        this.removeEventsSuscribed();
        
        // Check if response had errors
        let errorItems:any[] = response && response.data ? response.data.filter( item => item.status && (item.status.toLowerCase() == "error" || item.status.toLowerCase() == "server_error") ) : [];
        if(errorItems && errorItems.length){
            let errorCode:number = errorItems[0].code;
            this.sendError(errorCode || LegaErrors.NETWORK_ERROR);
            return;
        }
        
        let settlePlay:any = ObjectUtil.findByMethod(response, "settlePlay");
        if(settlePlay){
            let model:LegaModel = LegaModel.getInstance();
            model.balance = settlePlay.balance;
            model.freeBalance = settlePlay.freeBalance;
        }
        
        this.params.response = response;

        this.sendComplete();
    }

    private executeActionOnFail(errorCode:number): void {
        this.removeEventsSuscribed();
                
        this.sendError(errorCode);
    }

    private removeEventsSuscribed(): void {
        leanderGMApi.unsubscribeFromEvent(this.serverRequestCompleted);
        this.serverRequestCompleted = null;

        leanderGMApi.unsubscribeFromEvent(this.serverRequestFailed);
        this.serverRequestFailed = null;
    }
}