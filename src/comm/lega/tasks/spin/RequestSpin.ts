import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ObjectUtil } from "../../../../utils/ObjectUtil";
import { LegaModel } from "../../LegaModel";
import { CommSpinResponse, CommSpinWinnings, CommSpinWinningsLine } from "../../../../types/CommSpinResponse";
import { CommResponseStates } from "../../../../types/CommResponseStates";
import { LegaErrors } from "../../LegaErrors";

declare var leanderGMApi:any;

/** @internal */
export class RequestSpin extends ConcreteTask {
    private playRequestCompleted: number;
    private playRequestDenied: number;
    private serverRequestCompleted: number;
    private serverRequestFailed: number;

    execute(params:any = null): void{
        let sessionId = leanderGMApi.getSessionId();

        let betParams:any = {};
        
        // Add lines
        betParams.linesPlayed = this.params.lines != null ? this.params.lines : 0;

        // Add any extra params that was passed on to the method
        if(this.params.extraParams != null){
            for(var paramName in this.params.extraParams){
                betParams[paramName] = this.params.extraParams[paramName];
            }
        }

        let requestData: Array<any> = [
            {
                "method": "openPlay",
                "params": {
                    "sessionId": sessionId
                }
            },
            {
                "method": "placeBet",
                "params": {
                    "sessionId": sessionId,
                    "type": "spin",
                    "bet": this.params.bet,
                    "params": betParams
                }
            },
            {
                "method": "getBetResult",
                "params": {
                    "sessionId": sessionId
                }
            }
        ];
        
        // Add save data (if it has any)
        if(this.params.saveData != null && this.params.saveData.length){
            for(let i = 0; i < this.params.saveData.length; i++){
                requestData.push({
                    "method": "saveData",
                    "params": {
                        "data": this.params.saveData[i].data,
                        "name": this.params.saveData[i].name,
                        "sessionId": sessionId
                    }
                });
            }
        }

        this.playRequestCompleted = leanderGMApi.subscribeToEvent(
            leanderGMApi.publications.PLAY_CONFIRMED,
            () => {
                this.serverRequestCompleted = leanderGMApi.subscribeToEvent(
                    leanderGMApi.requestStatus.REQUEST_COMPLETED,
                    this.executeActionOnSuccess.bind(this)
                );
        
                this.serverRequestFailed = leanderGMApi.subscribeToEvent(
                    leanderGMApi.requestStatus.REQUEST_FAILED,
                    this.executeActionOnFail.bind(this, LegaErrors.NETWORK_ERROR)
                );
        
                leanderGMApi.sendRequest(requestData);
            }
        );

        this.playRequestDenied = leanderGMApi.subscribeToEvent(
            leanderGMApi.publications.PLAY_DENIED,
            this.executeActionOnFail.bind(this, LegaErrors.TEMPORARILY_UNAVAILABLE)
        );

        leanderGMApi.publishEvent(leanderGMApi.publications.PLAY_REQUEST);
    }
    
    private executeActionOnSuccess(response: any): void {
        this.removeEventsSuscribed();
        
        let getBetResult:any = ObjectUtil.findByMethod(response, "getBetResult");
        if(getBetResult){
            let model:LegaModel = LegaModel.getInstance();
            model.balance = getBetResult.balance;
            model.freeBalance = getBetResult.freeBalance;
            model.parseFreePlays(getBetResult.params[0].free_plays_data || getBetResult.params[0].freePlaysData);
            model.parseJackpots(getBetResult.params[0].jackpotsInfo);

            let result:any = getBetResult.params[0];

            if(result){
                this.params.response = RequestSpin.parseSpinResponse(result);
            }

            this.sendComplete();
        } else {
            let error:any = response.data.filter((e:any) => e.hasOwnProperty("errorCode") && e.errorCode.length);
            if(error != null && error instanceof Array && error.length){
                this.sendError(error[0].code || LegaErrors.SERVER_ERROR);
            } else {
                this.sendError(LegaErrors.SERVER_ERROR);
            }
        }
    }

    private executeActionOnFail(errorCode: number): void {
        this.removeEventsSuscribed();
        
        this.sendError(errorCode);
    }

    private removeEventsSuscribed(): void {
        leanderGMApi.unsubscribeFromEvent(this.serverRequestCompleted);
        this.serverRequestCompleted = null;

        leanderGMApi.unsubscribeFromEvent(this.serverRequestFailed);
        this.serverRequestFailed = null;

        leanderGMApi.unsubscribeFromEvent(this.playRequestCompleted);
        this.playRequestCompleted = null;

        leanderGMApi.unsubscribeFromEvent(this.playRequestDenied);
        this.playRequestDenied = null;
    }

    static parseSpinResponse(result:any): CommSpinResponse {
        let model:LegaModel = LegaModel.getInstance();

        let ret:CommSpinResponse = {
            creditsWon: result.creditsWon,
            reel: (result.reelStops || result.reelLayout),
            reelKey: (result.reelKey || result.reelKeys || null),
            winnings: RequestSpin.getWinnings(result.winningLines),
            bonus: result.bonusInfo,
            freePlays: null,
            getBonus: (name:string): any => {
                for(var bonus in result.bonusInfo){
                    if(result.bonusInfo[bonus].bonusName == name){
                        return result.bonusInfo[bonus];
                    }
                }
                return null;
            },
            getProp: (name:string): any => {
                return result.hasOwnProperty(name) ? result[name] : null;
            },
            type: CommResponseStates.SPIN
        };

        if(model.freePlays != null){
            ret.freePlays = model.freePlays;
        }

        return ret;
    }

    private static getWinnings(result:any): CommSpinWinnings {
        if(result == null) return null;

        // Any value not here will be considered as an extraData
        let valuesUsed:string[] = ["creditsWon", "count", "line", "matchPositions", "symbol", "multiplier"];

        let ret:CommSpinWinnings = {
            creditsWon: result.creditsWon,
            lines: []
        };

        for(let i = 0; i < result.lines.length; i++){
            let line:CommSpinWinningsLine = {
                creditsWon: result.lines[i].creditsWon,
                count: result.lines[i].count,
                line: result.lines[i].line,
                positions: result.lines[i].matchPositions,
                symbol: result.lines[i].symbol,
                multiplier: result.lines[i].multiplier || 1,
                extraData: {}
            };

            for(var param in result.lines[i]){
                if(valuesUsed.indexOf(param) < 0){
                    line.extraData[param] = result.lines[i][param];
                }
            }

            ret.lines.push(line);
        }

        return ret;
    }
}