import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";
import { CommInfoResponse } from "../../../../types/CommInfoResponse";

declare var leanderGMApi:any;

/** @internal */
export class RegisterInfoHandler extends ConcreteTask {
    private model:LegaModel = LegaModel.getInstance();
    private optionsEvent:number;
    private aboutEvent:number;
    private paytableEvent:number;
    private helpEvent:number;

    execute(params:any = null):void {        
        this.optionsEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.CONTROL_PANEL_STATUS_CHANGED, this.handleOptionsChange.bind(this));
        if(this.params.useCustomAboutScreen){
            this.aboutEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.ABOUT_GAME_STATUS_CHANGED, this.handleAboutChange.bind(this));
        }
        this.paytableEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.PAY_TABLE_STATUS_CHANGED, this.handlePaytableChange.bind(this));
        this.helpEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.HELP_STATUS_CHANGED, this.handleHelpChange.bind(this));

        this.sendComplete();
    }
    
    private handleOptionsChange(data: any): void {
        this.model.options = data.data;
        
        leanderGMApi.publishEvent(leanderGMApi.publications.CONTROL_PANEL_STATUS_CHANGED_GAME, this.model.options);

        this.notify();
    }
    
    private handleAboutChange(data: any): void {
        this.model.about = data.data;
        
        leanderGMApi.publishEvent(leanderGMApi.publications.ABOUT_GAME_STATUS_CHANGED_GAME, this.model.about);

        this.notify();
    }

    private handlePaytableChange(data: any): void {
        this.model.paytable = data.data;

        leanderGMApi.publishEvent(leanderGMApi.publications.PAY_TABLE_STATUS_CHANGED_GAME, this.model.paytable);

        this.notify();
    }

    private handleHelpChange(data: any): void {
        this.model.help = data.data;

        leanderGMApi.publishEvent(leanderGMApi.publications.HELP_STATUS_CHANGED_GAME, this.model.help);

        this.notify();
    }
    
    private notify(): void {
        let info:CommInfoResponse = {
            controlPanel: this.model.options,
            about: this.model.about,
            paytable: this.model.paytable,
            help: this.model.help
        };

        this.params.onInfoUpdate(info);
    }
}