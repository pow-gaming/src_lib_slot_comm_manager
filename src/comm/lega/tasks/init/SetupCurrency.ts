import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { CurrencyConfig } from "../../../../utils/types/CurrencyConfig";

declare var leanderGMApi:any;

/** @internal */
export class SetupCurrency extends ConcreteTask {
    execute(params:any = null): void {
        var currencyFormatting = leanderGMApi.getCurrencyFormatting();

        let currencyConfig:CurrencyConfig = {
            mask: this.createMask(this.params.currencyConfig),
            config: {
                delimiters: {
                    thousands: currencyFormatting.groupingSeparator,
                    decimal: currencyFormatting.decimalSeparator
                },
                abbreviations: {
                    thousand: 'k',
                    million: 'm',
                    billion: 'b',
                    trillion: 't'
                },
                currency: {
                    symbol: this.getCurrencySymbol(this.params.currencyConfig)
                }
            }
        };

        this.params.currencyConfig = currencyConfig;

        this.sendComplete();
    }

    private createMask(currency: any): string {
        var mask: string = "0,0."

        //Add decimal zeros
        for (var i = 0; i < currency.currencyDecimalPlaces; i++) {
            mask += "0";
        }

        //Add symbol place
        //assert(!(currency.currencyPrefix.length > 0 && currency.currencySuffix.length > 0), "SetupCurrencyCommand:: Currency has both, suffix and prefix.");
        if (currency.currencyPrefix.length > 0) {
            mask = "$" + mask;
        } 
        // It can have both prefix and suffix (why not?)
        if (currency.currencySuffix.length > 0) {
            mask += "$";
        }

        return mask;
    }
    
    private getCurrencySymbol(currency: any): string {
        return currency.currencyPrefix + currency.currencySuffix;
    }
}