import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { CommErrorResponse } from "../../../../types/CommErrorResponse";
import { LegaModel } from "../../LegaModel";

declare var leanderGMApi:any;

/** @internal */
export class RegisterErrorHandler extends ConcreteTask {
    private _model:LegaModel = LegaModel.getInstance();

    execute(params:any = null):void {        
        leanderGMApi.subscribeToEvent(leanderGMApi.publications.ERROR_HANDLED, this.handleErrorResponse.bind(this));

        this.sendComplete();
    }
    
    private handleErrorResponse(data: any): void {
        let error:CommErrorResponse = {
            code: data.data.code,
            userConfirmed: data.data.confirmation,
            userCancelled: !data.data.confirmation,
            isCritical: data.data.criticalType,
            message: data.data.message
        };

        if(this._model.errorCode == data.data.code){
            this._model.errorCode = null;
        }

        this.params.onErrorHandled(error);
    }
}