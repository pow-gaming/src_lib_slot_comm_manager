import * as Q from 'q';
import { URLVars } from "./../../../../utils/URLVars";
import { ConcreteTask } from '../../../../utils/task/ConcreteTask';

declare var leanderGMApi:any;

/** @internal */
export class CreateLeGaAPI extends ConcreteTask {
    protected apiFile: string = "leandergmapi/4/LeanderApiLoader.php";

    execute(params: any = null): void {
        super.execute(params);
   
        let apiPath: string = decodeURIComponent(URLVars.getQueryVariable("apiAddress"));

        if (apiPath && apiPath != "undefined") {
            // Remove special chars that are not valid for URL's
            var regExp = new RegExp("/[\"'<>=?\[\]\{\}]/gi");
            if (regExp.test(apiPath)) {
                // URL not valid. Try relative path
                apiPath = "../../";
            }
        } else {
            // URL not valid. Try relative path
            apiPath = "../../";
        }

        if(this.apiFile.substr(0, 1) == "/"){
            // If it starts with "/", it needs to be at the root of the apiPath
            apiPath = apiPath.substr( 0, apiPath.indexOf("/", apiPath.indexOf("://") + 3) ); 
        }

        let apiScript = document.createElement("script");
        apiScript.type = "text/javascript";
        apiScript.src = apiPath + this.apiFile;

        this.addScript(apiScript);

        this.checkInit().then(this.sendComplete.bind(this));
    }
    
    private addScript(scriptUrl:HTMLScriptElement):void {
        if(document.body){
            document.body.appendChild(scriptUrl);
        } else {
            window.requestAnimationFrame(() => this.addScript(scriptUrl));
        }
    }
    
    protected checkInit(): Q.IPromise<void> {
        let deferred = Q.defer<void>();

        let intervalId = setInterval( () => {
            if ((typeof (leanderGMApi) != "undefined") && leanderGMApi && leanderGMApi.initialized) {
                clearInterval(intervalId);
                
                leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_STARTED);

                deferred.resolve();
            }
        }, 500 );

        return deferred.promise;
    }
}