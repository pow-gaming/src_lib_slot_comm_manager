
import { Signal } from 'signals';
import { ConcreteTask } from '../../../../utils/task/ConcreteTask';
import { CommActionData } from '../../../../types/CommActionData';
import { CommActionType } from '../../../../types/CommActionType';

declare var leanderGMApi:any;

/** @internal */
export class RegisterActionsHandler extends ConcreteTask {

    private allowedActions:any = {};

    onAction:Signal = new Signal();

    execute(params:any = null):void {        

        leanderGMApi.subscribeToEvent(leanderGMApi.publications.EXECUTE_ACTION_GAME, this.handleExecuteActionGame.bind(this));
        
        this.allowedActions = {
            [leanderGMApi.gameActionsToExecute.DEFAULT.action]: "DEFAULT",
            [leanderGMApi.gameActionsToExecute.MAX_BET.action]: "MAX_BET",
            [leanderGMApi.gameActionsToExecute.FAST_PLAY.action]: "FAST_PLAY",
            [leanderGMApi.gameActionsToExecute.INCREASE_BET.action]: "INCREASE_BET",
            [leanderGMApi.gameActionsToExecute.DECREASE_BET.action]: "DECREASE_BET",
            [leanderGMApi.gameActionsToExecute.CHANGE_BET.action]: "CHANGE_BET",
            [leanderGMApi.gameActionsToExecute.INCREASE_LINES.action]: "INCREASE_LINES",
            [leanderGMApi.gameActionsToExecute.DECREASE_LINES.action]: "DECREASE_LINES",
            [leanderGMApi.gameActionsToExecute.CHANGE_LINES.action]: "CHANGE_LINES",
            [leanderGMApi.gameActionsToExecute.GENERIC_BUTTON_1.action]: "GENERIC_BUTTON_1",
            [leanderGMApi.gameActionsToExecute.GENERIC_BUTTON_2.action]: "GENERIC_BUTTON_2",
            [leanderGMApi.gameActionsToExecute.GENERIC_BUTTON_3.action]: "GENERIC_BUTTON_3",
            [leanderGMApi.gameActionsToExecute.GENERIC_BUTTON_4.action]: "GENERIC_BUTTON_4",
            [leanderGMApi.gameActionsToExecute.GENERIC_BUTTON_5.action]: "GENERIC_BUTTON_5",
            [leanderGMApi.gameActionsToExecute.GENERIC_BUTTON_6.action]: "GENERIC_BUTTON_6",
        };
        
        this.sendComplete();
    }

    public handleExecuteActionGame(action:{data:string}):void {
        if(this.allowedActions.hasOwnProperty(action.data)){
            let actionName:string = this.allowedActions[action.data];
            let data:CommActionData = {
                action: (<any>CommActionType)[actionName],
                confirmation: this.confirmation.bind(this, action.data)
            }
            
            if(this.params.hasOwnProperty("onActionRequested")){
                this.params.onActionRequested(data);
            } else {
                this.confirmation(action.data, false);
            }
        }
    }

    private confirmation(action:string, success:boolean):void {
        leanderGMApi.publishEvent(success? leanderGMApi.publications.EXECUTE_ACTION_GAME_CONFIRMED : leanderGMApi.publications.EXECUTE_ACTION_GAME_IGNORED, action);
    }
}


