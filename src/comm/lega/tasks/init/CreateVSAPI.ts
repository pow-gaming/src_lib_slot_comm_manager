import * as Q from 'q';
import { CreateLeGaAPI } from './CreateLeGaAPI';

declare var vsApi:any;
declare var GAME_NAME:any;

/** @internal */
export class CreateVSAPI extends CreateLeGaAPI {
    protected apiFile: string = "/vsapi/1/VSApiLoader.php";

    protected checkInit(): Q.IPromise<void> {
        let deferred = Q.defer<void>();

        let intervalId = setInterval( () => {
            if ((typeof (vsApi) != "undefined") && vsApi) {
                clearInterval(intervalId);
                
                vsApi.init(GAME_NAME);

                deferred.resolve();
            }
        }, 500 );

        return deferred.promise;
    }
}