import { ObjectUtil } from "../../../../utils/ObjectUtil";
import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";

declare var leanderGMApi:any;

/** @internal */
export class RegisterBalanceHandler extends ConcreteTask {
    
    execute(params:any = null):void {        
        leanderGMApi.subscribeToEvent(leanderGMApi.publications.BALANCE_UPDATED, this.handleBalanceUpdate.bind(this));

        this.sendComplete();
    }
    
    private handleBalanceUpdate(data: any): void {
        let model:LegaModel = LegaModel.getInstance();

        let balance:number = parseFloat(ObjectUtil.getObjectByPath(data, "data.balance"));
        let freeBalance:number = parseFloat(ObjectUtil.getObjectByPath(data, "data.free_balance"));
        
        model.balance = balance;
        model.freeBalance = freeBalance;

        this.params.onBalanceUpdate({
            balance: balance,
            freeBalance: freeBalance
        });
    }
}