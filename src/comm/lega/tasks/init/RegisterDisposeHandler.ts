import { AbstractTask } from "../../../../utils/task/AbstractTask";

declare var leanderGMApi:any;

/** @internal */
export class RegisterDisposeHandler extends AbstractTask {
    private disposeApplicationEvent:number;

    execute(params:any = null): void {
        // Listen to a DISPOSE APLICATION event
        this.disposeApplicationEvent = leanderGMApi.subscribeToEvent(
            leanderGMApi.publications.DISPOSE_APPLICATION,
            this.executeDisposeApplication.bind(this)
        );

        this.sendComplete();
    }
    
    private executeDisposeApplication() {
        // Stop listening to DISPOSE APPLICATION event
        leanderGMApi.unsubscribeFromEvent(this.disposeApplicationEvent);

        // Perform operations in the game
        

        // Publish the GAME READY FOR UNLOAD event
        leanderGMApi.publishEvent(
            leanderGMApi.publications.GAME_READY_FOR_UNLOAD
        );
    }
}