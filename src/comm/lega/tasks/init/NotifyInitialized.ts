import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";

declare var leanderGMApi:any;

/** @internal */
export class NotifyInitialized extends ConcreteTask {
    private model:LegaModel = LegaModel.getInstance();

    private subscribeInitConfirmation:number;
    private subscribePromotionCancelled:number;

    private hasPromotions:boolean;
    private hasHistory:boolean;

    execute(params:any = null): void {
        // This is the time when the user can cancel a promotion opportunity
        this.hasPromotions = this.model.freePlays != null;
        this.hasHistory = this.model.history != null;

        this.subscribePromotionCancelled = leanderGMApi.subscribeToEvent(
            leanderGMApi.publications.PROMOTION_CANCELLED, this.handlePromotionCanceled.bind(this)
        );

        this.subscribeInitConfirmation = leanderGMApi.subscribeToEvent(
            leanderGMApi.publications.INITIALIZED_CONFIRMED, this.handleInitializedConfirmation.bind(this)
        );

        leanderGMApi.publishEvent(leanderGMApi.publications.INITIALIZED);
    }

    private handleInitializedConfirmation(): void {
        // If user is recovering, and had a promotion opportunity, but cancelled it, it will enter this way.
        if(this.hasPromotions && this.hasHistory && !leanderGMApi.gamePromotionActive){
            this.handlePromotionCanceled();
        }

        leanderGMApi.unsubscribeFromEvent(this.subscribeInitConfirmation);
        leanderGMApi.unsubscribeFromEvent(this.subscribePromotionCancelled);
        
        this.sendComplete();
    }

    private handlePromotionCanceled(): void {
        if(this.hasPromotions){
            this.model.clearFreePlays();
            this.hasPromotions = false;
        }
    }
}