import { ObjectUtil } from "../../../../utils/ObjectUtil";
import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { CommInitResponse } from "../../../../types/CommInitResponse";
import { LegaModel } from "../../LegaModel";
import { CommHistory } from "../../../../types/CommHistory";
import { RequestSpin } from "../spin/RequestSpin";
import { RequestBonus } from "../bonus/RequestBonus";
import { CommResponseStates } from "../../../../types/CommResponseStates";
import { LegaErrors } from "../../LegaErrors";
import * as Q from 'q';
import { RequestGamble } from "../gamble/RequestGamble";

declare var leanderGMApi:any;
declare var leanderWrapperEnvironmentData:any;

/** @internal */
export class InitLeGa extends ConcreteTask {

    private serverRequestCompleted: any;
    private serverRequestFailed: any;

    execute(params:any = null): void {
        leanderGMApi.publishEvent(leanderGMApi.publications.GAME_VERSION_UPDATE, this.params.version);
        leanderGMApi.publishEvent(leanderGMApi.publications.GAME_STATUS_CHANGED, leanderGMApi.gameStates.INITIALIZE);

        if (this.serverRequestCompleted != null) {
            this.serverRequestCompleted = null;
        }
        this.serverRequestCompleted = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_COMPLETED,
            (response:any) => this.executeActionOnSuccess.call(this, response)
        );

        if (this.serverRequestFailed != null) {
            this.serverRequestFailed = null;
        }
        this.serverRequestFailed = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_FAILED,
            (response:any) => this.executeActionOnFail.call(this, response)
        );

        let dataToSend = {
            "method": "initializeGame",
            "params": {
                "sessionId": leanderGMApi.sessionData.sessionId,
                "languageId": leanderGMApi.gameOptions.language
            }
        }

        leanderGMApi.sendRequest(dataToSend);
    }
    
    private executeActionOnSuccess(response: any) {
        if(!this.checkError(response)) {
            leanderGMApi.unsubscribeFromEvent(this.serverRequestCompleted);
            leanderGMApi.unsubscribeFromEvent(this.serverRequestFailed);
            
            this.serverRequestFailed = null;
            this.serverRequestCompleted = null;
    
            leanderGMApi.setUseCustomJackpotBar(true);
            
            this.params.currencyConfig = {
                currencyDecimalPlaces : ObjectUtil.getObjectByPath(response, "data[0].params.currencyDecimalPlaces"),
                currencyPrefix : ObjectUtil.getObjectByPath(response, "data[0].params.currencyPrefix"),
                currencySuffix : ObjectUtil.getObjectByPath(response, "data[0].params.currencySuffix")
            };

            let model:LegaModel = LegaModel.getInstance();
            let historyData:CommHistory = this.parseHistory(ObjectUtil.getObjectByPath(response, "data[0].params.responseHistory"));

            // Check if play has been interrupted, and not in the expected way.
            // If it did, try to recover it and continue parsing.
            this.checkForMissedData(response).done( (response) => {
                model.balance = ObjectUtil.getObjectByPath(response, "data[0].balance");
                model.freeBalance = ObjectUtil.getObjectByPath(response, "data[0].freeBalance");
                model.history = historyData;
                model.bet = ObjectUtil.getObjectByPath(response, "data[0].params.betPerLineDefault");
                model.lines = ObjectUtil.getObjectByPath(response, "data[0].params.lineLayout") ? ObjectUtil.getObjectByPath(response, "data[0].params.lineLayout").length : null;
                model.parseFreePlays(ObjectUtil.getObjectByPath(response, "data[0].freePlaysData"));
    
                let jackpotList:any[] = ObjectUtil.getObjectByPath(response, "data[0].jackpotsInfo");
                if(jackpotList && jackpotList.length){
                    model.parseJackpots(jackpotList);
                }
    
                this.params.savedData = ObjectUtil.getObjectByPath(response, "data[0].data");
    
                // Create response object
                let responseData:CommInitResponse = {
                    balance: {
                        balance: ObjectUtil.getObjectByPath(response, "data[0].balance"),
                        freeBalance: ObjectUtil.getObjectByPath(response, "data[0].freeBalance")
                    },
                    autoplay: {
                        value: ObjectUtil.getObjectByPath(response, "data[0].params.autoPlayDefault"),
                        list: leanderGMApi.gameAutoPlayList
                    },
                    history: historyData,
                    bet: {
                        value: ObjectUtil.getObjectByPath(response, "data[0].params.betPerLineDefault"),
                        list: ObjectUtil.getObjectByPath(response, "data[0].params.betPerLine")
                    },
                    rtp: {
                        game: ObjectUtil.getObjectByPath(response, "data[0].gameRTP"),
                        jackpot: ObjectUtil.getObjectByPath(response, "data[0].gameRTPJP"),
                        upper: ObjectUtil.getObjectByPath(response, "data[0].gameRTPUP")
                    },
                    freePlays: model.freePlays,
                    jackpots: model.jackpots,
                    lines: ObjectUtil.getObjectByPath(response, "data[0].params.lineLayout"),
                    lobby: (leanderGMApi.getLobbyActive() ? leanderGMApi.goToLobby.bind(leanderGMApi) : null),
                    gameRules: (leanderGMApi.getGameRulesActive() ? leanderGMApi.showGameRules.bind(leanderGMApi) : null),
                    keyboardEnabled: model.keyboardEnabled,
                    logoEnabled: (typeof(leanderWrapperEnvironmentData) != "undefined") && ( !leanderWrapperEnvironmentData.hasOwnProperty("gameLogoDisabled") || leanderWrapperEnvironmentData.gameLogoDisabled.toString() == "false" ),
                    customConfig: {
                        customAssets: ((typeof(leanderWrapperEnvironmentData) != "undefined") && ( leanderWrapperEnvironmentData.hasOwnProperty("customAssets")) ?  leanderWrapperEnvironmentData.customAssets : "default"),
                        customIndex: ((typeof(leanderWrapperEnvironmentData) != "undefined") && ( leanderWrapperEnvironmentData.hasOwnProperty("customIndex")) ?  leanderWrapperEnvironmentData.customIndex : -1)
                    },
                    extraParams: this.getExtraParams(response, this.params.extraParams),
                    type: CommResponseStates.INIT
                }
                
                this.params.responseData = responseData;
                
                this.sendComplete();
            }, (error) => {
                this.sendCanceled(LegaErrors.NETWORK_ERROR);
            });
        } else {
            let errorCode:number = null;
            for (var i in response.data) {
                errorCode = (response.data != null && response.data[i] != null) ? response.data[i].code : null;
                if(errorCode != null) break;
            }

            this.sendCanceled(errorCode);
        }
    }

    private executeActionOnFail(response:any): void {        
        this.sendCanceled(LegaErrors.NETWORK_ERROR);
    }

    private getExtraParams(response:any, extraParams:any): any {
        let ret = null;
        
        if(extraParams && extraParams.length){            
            ret = {};
            for(var i = 0; i < extraParams.length; i++){
                var paramName:string = extraParams[i];
                (<any>ret)[paramName] = ObjectUtil.getObjectByMultiplePaths(response, ["data[0]." + paramName, "data[0].params." + paramName]);
            }
        }

        return ret;
    }

    private parseHistory(response:any): CommHistory {
        if(response == null) return null;
        
        let ret:CommHistory = {
            history:[]
        }

        for(let i = 0; i < response.length; i++){
            if(response[i].method == "getBetResult"){
                if(response[i].params[0].type == "spin"){
                    ret.history.push(RequestSpin.parseSpinResponse(response[i].params[0]));
                } else 
                if(response[i].params[0].type == "bonus"){
                    ret.history.push(RequestBonus.parseBonusResponse(response[i].params[0]));
                } else 
                if(response[i].params[0].type == "gamble"){
                    ret.history.push(RequestGamble.parseGambleResponse(response[i].params[0]));
                } else {
                    ret.history.push(response[i].params[0]);
                }
            }
        }

        return ret;
    }

    private checkForMissedData(response: any):Q.Promise<any> {
        let defer:Q.Deferred<any> = Q.defer<any>();

        let historyData:any = ObjectUtil.getObjectByPath(response, "data[0].params.responseHistory");
        
        if (historyData != null && historyData.length > 0) {
            // Needs to send an extra getBetResult because of missed data?
            let bonusInfo:any = ObjectUtil.findProp(response, "bonusInfo");
            let recoverMissData:boolean = true;
            
            historyData.forEach((param:any) => {
                if(param.method && param.method == "getBetResult") {
                    recoverMissData = false;
                }
            });

            if (bonusInfo == null && recoverMissData) {
                var recoverRequestCompleted:number = leanderGMApi.subscribeToEvent(leanderGMApi.requestStatus.REQUEST_COMPLETED, (data:any) => {
                    leanderGMApi.unsubscribeFromEvent(recoverRequestCompleted);
                    leanderGMApi.unsubscribeFromEvent(recoverRequestFailed);

                    historyData.push(ObjectUtil.getObjectByPath(data, "data[0]"));

                    response.data[0].params.responseHistory = historyData;

                    defer.resolve(response);
                });
        
                var recoverRequestFailed:number = leanderGMApi.subscribeToEvent(leanderGMApi.requestStatus.REQUEST_FAILED, (response:any) => {
                    leanderGMApi.unsubscribeFromEvent(recoverRequestCompleted);
                    leanderGMApi.unsubscribeFromEvent(recoverRequestFailed);

                    defer.reject();
                });

                leanderGMApi.sendRequest([{ "method": "getBetResult", "params": { "sessionId": leanderGMApi.getSessionId() } } ]);
            } else {
                defer.resolve(response);
            }
        } else {
            defer.resolve(response);
        }

        return defer.promise;
    }

    private checkError(response: any): boolean {
        for (var i in response.data) {
            var statusErrorId = (response.data != null && response.data[i] != null) ? response.data[i].status : null;
            if (statusErrorId == "ERROR" || statusErrorId == "SERVER_ERROR") {
                return true;
            }
        }
        return false;
    }

}