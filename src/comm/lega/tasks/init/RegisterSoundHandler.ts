import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";
import { CommSoundResponse } from "../../../../types/CommSoundResponse";

declare var leanderGMApi:any;

/** @internal */
export class RegisterSoundHandler extends ConcreteTask {
    private model:LegaModel = LegaModel.getInstance();
    private soundEvent:number;
    private musicEvent:number;
    private sfxEvent:number;

    execute(params:any = null):void {        
        this.soundEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.GENERAL_SOUND_STATUS_CHANGED, this.handleGeneralSoundChange.bind(this));
        this.musicEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.MUSIC_STATUS_CHANGED, this.handleMusicChange.bind(this));
        this.sfxEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.SOUND_EFFECTS_STATUS_CHANGED, this.handleSFXChange.bind(this));

        this.sendComplete();
    }
    
    private handleGeneralSoundChange(data: any): void {
        // Do not replicate on other events
        leanderGMApi.unsubscribeFromEvent(this.musicEvent);
        leanderGMApi.unsubscribeFromEvent(this.sfxEvent);

        this.model.generalSound = data.data;

        // Update on casino
        leanderGMApi.publishEvent(leanderGMApi.publications.GENERAL_SOUND_STATUS_CHANGED_GAME, this.model.generalSound);

        // If music or sfx was in a different state than the new general sound state, change it and notify it.
        if( (this.model.generalSound && !this.model.music) || (!this.model.generalSound && this.model.music) ){
            this.handleMusicChange({data: this.model.generalSound});
        }
        if( (this.model.generalSound && !this.model.sfx) || (!this.model.generalSound && this.model.sfx) ){
            this.handleSFXChange({data: this.model.generalSound});
        }

        this.notify();

        // If I subscribe the events again immediately, they will be dispatched, so wait a bit to let them getting dispatched without warning the user
        setTimeout(() => {
            this.musicEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.MUSIC_STATUS_CHANGED, this.handleMusicChange.bind(this));
            this.sfxEvent = leanderGMApi.subscribeToEvent(leanderGMApi.publications.SOUND_EFFECTS_STATUS_CHANGED, this.handleSFXChange.bind(this));
        }, 10);
    }
    
    private handleMusicChange(data: any): void {
        this.model.music = data.data;
        
        // If both music and sfx have the same value, and it's different from the general value, change it
        if(this.model.music == this.model.sfx && this.model.music != this.model.generalSound){
            this.handleGeneralSoundChange({data: this.model.music});
        }

        leanderGMApi.publishEvent(leanderGMApi.publications.MUSIC_STATUS_CHANGED_GAME, this.model.music);

        this.notify();
    }

    private handleSFXChange(data: any): void {
        this.model.sfx = data.data;

        // If both music and sfx have the same value, and it's different from the general value, change it
        if(this.model.sfx == this.model.music && this.model.sfx != this.model.generalSound){
            this.handleGeneralSoundChange({data: this.model.sfx});
        }
        
        leanderGMApi.publishEvent(leanderGMApi.publications.SOUND_EFFECTS_STATUS_CHANGED_GAME, this.model.sfx);

        this.notify();
    }
    
    private notify(): void {
        let sound:CommSoundResponse = {
            general: this.model.generalSound,
            music: this.model.music,
            sfx: this.model.sfx
        };

        this.params.onSoundUpdate(sound);
    }
}