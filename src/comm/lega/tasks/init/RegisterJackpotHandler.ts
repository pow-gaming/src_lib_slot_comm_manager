import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";

declare var leanderGMApi:any;

/** @internal */
export class RegisterJackpotHandler extends ConcreteTask {

    execute(params:any = null): void {
        let model:LegaModel = LegaModel.getInstance();

        leanderGMApi.subscribeToEvent(leanderGMApi.publications.JACKPOT_UPDATED, this.handleJackpotUpdate.bind(this));

        model.onJackpotUpdate.add(this.handleJackpotUpdate, this);

        this.sendComplete();
    }
    
    private handleJackpotUpdate(data: any): void {
        let model:LegaModel = LegaModel.getInstance();

        if(data){
            model.onJackpotUpdate.remove(this.handleJackpotUpdate, this);
            
            let jackpotList:any[] = data.data.jackpotItems;
            
            model.parseJackpots(jackpotList);

            model.onJackpotUpdate.add(this.handleJackpotUpdate, this);
        }

        if(model.jackpots.length){
            this.params.onJackpotUpdate && this.params.onJackpotUpdate(model.jackpots);
        }
    }
}