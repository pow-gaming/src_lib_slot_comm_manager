import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";

declare var leanderGMApi:any;

/** @internal */
export class SetupKeyboard extends ConcreteTask {
    private model:LegaModel = LegaModel.getInstance();
    
    execute(params:any = null):void {
        this.model.keyboardEnabled = leanderGMApi.keyboardInputEnabled;

        leanderGMApi.subscribeToEvent(leanderGMApi.publications.KEYBOARD_INPUT_ENABLED, this.onKeyboardInputChange.bind(this));
        
        this.sendComplete();
    }

    private onKeyboardInputChange(data:any):void {
        let value:boolean = data.data;

        this.model.keyboardEnabled = value;
    }
}