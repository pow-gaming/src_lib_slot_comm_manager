import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";

declare var leanderGMApi:any;

/** @internal */
export class RequestAutoPlay extends ConcreteTask {
    private autoPlayActivatedConfirmation: number;
    private autoPlayActivatedCancellation: number;
    private autoPlayLimitReached: number;
    private model:LegaModel = LegaModel.getInstance();
    
    /**
     * Autoplay in LEGA platform requires a pre-authorization that might change the number of spins or the bet amount.
     * @param params 
     */
    execute(params:any = null): void {
        this.autoPlayActivatedConfirmation = leanderGMApi.subscribeToEvent(leanderGMApi.publications.AUTOPLAY_ACTIVATED_CONFIRMATION, this.autoplayConfirmedHandler.bind(this));
        this.autoPlayActivatedCancellation = leanderGMApi.subscribeToEvent(leanderGMApi.publications.AUTOPLAY_ACTIVATED_CANCELLATION, this.autoplayCanceledHandler.bind(this));
        this.autoPlayLimitReached = leanderGMApi.subscribeToEvent(leanderGMApi.publications.AUTOPLAY_LIMIT_REACHED, this.autoplayLimitReachedHandler.bind(this));

        leanderGMApi.publishEvent(leanderGMApi.publications.AUTOPLAY_ACTIVATED_GAME, {
            spins: this.params.spins, totalBetList: this.params.betList
        });
    }

    private autoplayConfirmedHandler(response:any): void {
        this.removeEventsSuscribed(false);
        
        this.params.response = response.data.spins;
        this.model.autoplayActive = true;

        this.sendComplete();
    }

    private autoplayCanceledHandler(): void {
        this.removeEventsSuscribed(true);
        
        this.model.autoplayActive = false;

        this.sendError();
    }
    
    private autoplayLimitReachedHandler(): void {
        this.removeEventsSuscribed(true);

        this.model.autoplayActive = false;

        this.params.stopCallback && this.params.stopCallback();
        this.sendError();
    }
    
    private removeEventsSuscribed(removeLimit:boolean): void {
        if(this.autoPlayActivatedConfirmation != null)
            leanderGMApi.unsubscribeFromEvent(this.autoPlayActivatedConfirmation);
        this.autoPlayActivatedConfirmation = null;

        if(this.autoPlayActivatedCancellation != null)
            leanderGMApi.unsubscribeFromEvent(this.autoPlayActivatedCancellation);
        this.autoPlayActivatedCancellation = null;

        if(removeLimit){
            if(this.autoPlayLimitReached != null)
                leanderGMApi.unsubscribeFromEvent(this.autoPlayLimitReached);
            this.autoPlayLimitReached = null;
        }
    }
}