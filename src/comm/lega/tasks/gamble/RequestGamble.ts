import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { LegaModel } from "../../LegaModel";
import { ObjectUtil } from "../../../../utils/ObjectUtil";
import { CommResponseStates } from "../../../../types/CommResponseStates";
import { LegaErrors } from "../../LegaErrors";
import { CommGambleResponse } from "../../../../types/CommGambleResponse";

declare var leanderGMApi:any;

/** @internal */
export class RequestGamble extends ConcreteTask {
    private serverRequestCompleted: number;
    private serverRequestFailed: number;
    private model:LegaModel = LegaModel.getInstance();

    execute(params:any = null): void{
        let sessionId = leanderGMApi.getSessionId();

        let requestData: Array<any> = [
            {
                "method": "placeBet",
                "params": {
                    "sessionId": sessionId,
                    "type": "gamble",
                    "bet": this.params.bet,
                    "params": {
                        "gambleAction": true,
                        "linesPlayed": 1
                    }
                }
            },
            {
                "method": "getBetResult",
                "params": {
                    "sessionId": sessionId
                }
            }
        ];
        
        // Add save data (if it has any)
        if(this.params.saveData != null && this.params.saveData.length){
            for(let i = 0; i < this.params.saveData.length; i++){
                requestData.push({
                    "method": "saveData",
                    "params": {
                        "data": this.params.saveData[i].data,
                        "name": this.params.saveData[i].name,
                        "sessionId": sessionId
                    }
                });
            }
        }

        for(let param in this.params.params){
            requestData[0].params.params[param] = this.params.params[param];
        }

        this.serverRequestCompleted = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_COMPLETED,
            this.executeActionOnSuccess.bind(this)
        );

        this.serverRequestFailed = leanderGMApi.subscribeToEvent(
            leanderGMApi.requestStatus.REQUEST_FAILED,
            this.executeActionOnFail.bind(this, LegaErrors.NETWORK_ERROR)
        );

        // If on autoplay, need to bypass the total bet check 
        // (Lega won't let a bonus bet go forward if it's different from the main bet, and it's on autoplay)
        if(this.model.autoplayActive){
            leanderGMApi.publishEvent(leanderGMApi.publications.TOTALBET_BYPASS_CHECK, true);
        }

        leanderGMApi.sendRequest(requestData);
    }
    
    private executeActionOnSuccess(response: any): void {
        this.removeEventsSuscribed();
        
        let getBetResult:any = ObjectUtil.findByMethod(response, "getBetResult");
        if(getBetResult){
            let model:LegaModel = LegaModel.getInstance();
            model.balance = getBetResult.balance;
            model.freeBalance = getBetResult.freeBalance;
            model.parseJackpots(getBetResult.params[0].jackpotsInfo);
            
            this.params.response = RequestGamble.parseGambleResponse(getBetResult.params[0]);

            this.sendComplete();
        } else {
            let error:any = response.data.filter((e:any) => e.hasOwnProperty("errorCode") && e.errorCode.length);
            if(error != null && error instanceof Array && error.length){
                this.sendError(error[0].code || LegaErrors.SERVER_ERROR);
            } else {
                this.sendError(LegaErrors.SERVER_ERROR);
            }
        }
    }

    private executeActionOnFail(errorCode: number): void {
        this.removeEventsSuscribed();
       
        this.sendError(errorCode);
    }

    private removeEventsSuscribed(): void {
        leanderGMApi.unsubscribeFromEvent(this.serverRequestCompleted);
        this.serverRequestCompleted = null;

        leanderGMApi.unsubscribeFromEvent(this.serverRequestFailed);
        this.serverRequestFailed = null;

        // Restore bypass check
        if(this.model.autoplayActive){
            leanderGMApi.publishEvent(leanderGMApi.publications.TOTALBET_BYPASS_CHECK, false);
        }
    }

    static parseGambleResponse(response:any): CommGambleResponse {
        let ret:CommGambleResponse = {
            bonus: response.bonusInfo,
            getBonus: (name:string): any => {
                for(var bonus in response.bonusInfo){
                    if(response.bonusInfo[bonus].bonusName == name){
                        return response.bonusInfo[bonus];
                    }
                }
                return null;
            },
            type: CommResponseStates.GAMBLE
        };

        return ret;
    }
}