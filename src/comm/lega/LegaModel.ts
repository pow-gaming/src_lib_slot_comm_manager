import { SavedData } from "../../utils/types/SavedData";
import { CurrencyConfig } from "../../utils/types/CurrencyConfig";
import { CommGameFreePlays } from "../../types/CommGameFreePlays";
import { CommHistory } from "../../types/CommHistory";
import { CommGameJackpot } from "../../types/CommGameJackpot";
import Dictionary from "../../utils/Dictionary";
import { Signal } from "signals";

/** @internal */
export class LegaModel {
    private static instance:LegaModel;

    private _history: CommHistory;
    private _savedData: SavedData;
    private _currency: CurrencyConfig;
    private _balance: {
        balance: number,
        freeBalance: number
    };
    private _bet: number;
    private _lines: number;
    private _soundEnabled: boolean;
    private _musicEnabled: boolean;
    private _sfxEnabled: boolean;
    private _optionsEnabled: boolean;
    private _aboutEnabled: boolean;
    private _paytableEnabled: boolean;
    private _helpEnabled: boolean;
    private _autoplayActive: boolean;
    private _freePlays: CommGameFreePlays;
    private _freePlaysAccepted: boolean;
    private _keyboardEnabled: boolean;
    private _jackpots: Dictionary<string, CommGameJackpot>;
    private _currErrorCode: number;
    private _errorCodeQueue: number[];

    onJackpotUpdate:Signal;

    private constructor() {
        this._history = null;
        this._savedData = new SavedData();
        this._currency = null;
        this._balance = {
            balance : 0,
            freeBalance : 0
        };
        this._bet = 0;
        this._lines = 0;
        this._soundEnabled = false;
        this._musicEnabled = false;
        this._sfxEnabled = false;
        this._optionsEnabled = false;
        this._aboutEnabled = false;
        this._paytableEnabled = false;
        this._helpEnabled = false;
        this._autoplayActive = false;
        this._freePlays = null;
        this._freePlaysAccepted = true;
        this._jackpots = new Dictionary<string, CommGameJackpot>((k => k));
        this._keyboardEnabled = null;
        this._currErrorCode = null;
        this._errorCodeQueue = [];
        this.onJackpotUpdate = new Signal();
    }

    static getInstance():LegaModel {
        if(!LegaModel.instance){
            LegaModel.instance = new LegaModel();
        }

        return LegaModel.instance;
    }

    get savedData():SavedData {
        return this._savedData;
    }

    set bet(value:number) {
        this._bet = value;
    }

    get bet():number {
        return this._bet;
    }

    set lines(value:number) {
        this._lines = value;
    }

    get lines():number {
        return this._lines;
    }

    get history():CommHistory {
        return this._history;
    }

    set history(value:CommHistory) {
        this._history = value;
    }

    set balance(value:number){
        this._balance.balance = value;
    }

    get balance():number {
        return this._balance.balance;
    }

    set freeBalance(value:number){
        this._balance.freeBalance = value;
    }

    get freeBalance():number {
        return this._balance.freeBalance;
    }

    get currencyConfig():CurrencyConfig {
        return this._currency;
    }

    set currencyConfig(value:CurrencyConfig) {
        this._currency = value;
    }

    get generalSound():boolean {
        return this._soundEnabled;
    }

    set generalSound(value:boolean) {
        this._soundEnabled = value;
    }

    get music():boolean {
        return this._musicEnabled;
    }

    set music(value:boolean) {
        this._musicEnabled = value;
    }

    get sfx():boolean {
        return this._sfxEnabled;
    }

    set sfx(value:boolean) {
        this._sfxEnabled = value;
    }

    get options():boolean {
        return this._optionsEnabled;
    }

    set options(value:boolean) {
        this._optionsEnabled = value;
    }

    get about():boolean {
        return this._aboutEnabled;
    }

    set about(value:boolean) {
        this._aboutEnabled = value;
    }

    get paytable():boolean {
        return this._paytableEnabled;
    }

    set paytable(value:boolean) {
        this._paytableEnabled = value;
    }

    get autoplayActive():boolean {
        return this._autoplayActive;
    }

    set autoplayActive(value:boolean) {
        this._autoplayActive = value;
    }

    get help():boolean {
        return this._helpEnabled;
    }

    set help(value:boolean) {
        this._helpEnabled = value;
    }

    set errorCode(value:number) {
        this._currErrorCode = value;
    }

    get errorCode():number {
        return this._currErrorCode;
    }

    enqueueError(errorCode:number):void {
        this._errorCodeQueue.push(errorCode);
    }

    dequeueError():number | null {
        if(this._errorCodeQueue.length){
            return this._errorCodeQueue.pop();
        } else {
            return null;
        }
    }

    parseFreePlays(data:any): void {
        if(data == null) return;
        if(!this._freePlaysAccepted) return;
        if(JSON.stringify(data) == "{}") return;

        let maxLines:number = (this.lines > 0 ? this.lines : null);
        this._freePlays = {
            bet: data.promotionTotalBet,
            lines: data.promotionMaxLines ? maxLines : data.promotionLines,
            totalWin: data.promotionWonSoFar,
            playsDone: data.promotionPlaysDone,
            playsLeft: data.promotionPlaysRemaining,
            playsTotal: (data.promotionPlaysDone + data.promotionPlaysRemaining)
        };
    }

    clearFreePlays():void {
        this._freePlays = null;
        this._freePlaysAccepted = false;
    }

    get freePlays():CommGameFreePlays {
        return this._freePlays;
    }

    parseJackpots(data:any[]): void {
        this._jackpots.clear();
        
        for(let i = 0; i < data.length; i++){
            let jackpot:CommGameJackpot = {
                active: data[i].active,
                awarded: data[i].awarded,
                contribution: data[i].contribution,
                currency: data[i].currency_id || data[i].currencyId,
                details: data[i].details,
                eligibleUsers: data[i].community_qty_eligible_users || data[i].communityEligibleUsers,
                id: data[i].jackpot_id || data[i].id,
                isDeferred: data[i].deferred_payment || data[i].deferredPayment,
                isProgressive: data[i].progressive,
                name: data[i].name,
                prize: data[i].progressive_accumulated_in_session_currency || data[i].progressiveAccumulated,
                seeding: data[i].seeding
            };

            this._jackpots.setValue(jackpot.id.toString(), jackpot);
        }

        this.onJackpotUpdate.dispatch();
    }

    set jackpots(value:CommGameJackpot[]) {    
        for(let i = 0; i < value.length; i++){
            this._jackpots.setValue(value[i].id.toString(), value[i]);
        }

        this.onJackpotUpdate.dispatch();
    }

    get jackpots():CommGameJackpot[] {
        let ret:CommGameJackpot[] = this._jackpots.values();
        ret = ret.sort((a, b) => ((a.id < b.id) ? -1 : 1));
        return ret;
    }

    set keyboardEnabled(value:boolean) {
        this._keyboardEnabled = value;
    }

    get keyboardEnabled():boolean {
        return this._keyboardEnabled;
    }
}