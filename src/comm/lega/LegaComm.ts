import * as Q from 'q';
import { IComm } from "../IComm";
import { SequenceTask } from '../../utils/task/SequenceTask';
import { ITask } from '../../utils/task/ITask';
import { CreateLeGaAPI } from './tasks/init/CreateLeGaAPI';
import { CreateVSAPI } from './tasks/init/CreateVSAPI';
import { RegisterJackpotHandler } from './tasks/init/RegisterJackpotHandler';
import { RegisterBalanceHandler } from './tasks/init/RegisterBalanceHandler';
import { InitLeGa } from './tasks/init/InitLeGa';
import { RegisterDisposeHandler } from './tasks/init/RegisterDisposeHandler';
import { SetupCurrency } from './tasks/init/SetupCurrency';
import { NotifyInitialized } from './tasks/init/NotifyInitialized';
import { SaveData } from './tasks/save/SaveData';
import { Signal } from 'signals';
import { SettleAndClosePlay } from './tasks/close/SettleAndClosePlay';
import { RequestSpin } from './tasks/spin/RequestSpin';
import { RequestBonus } from './tasks/bonus/RequestBonus';
import { RequestGamble } from './tasks/gamble/RequestGamble';
import { RequestAutoPlay } from './tasks/autoplay/RequestAutoPlay';
import { CommGameStates } from '../../types/CommGameStates';
import { CommInitRequest } from '../../types/CommInitRequest';
import { LegaModel } from './LegaModel';
import { CommGameFreePlaysModes } from '../../types/CommGameFreePlaysModes';
import { CommGameJackpot } from '../../types/CommGameJackpot';
import { RegisterErrorHandler } from './tasks/init/RegisterErrorHandler';
import { RegisterSoundHandler } from './tasks/init/RegisterSoundHandler';
import { RegisterInfoHandler } from './tasks/init/RegisterInfoHandler';
import { CommSavedResponse } from '../../types/CommSavedResponse';
import { ConcreteTask } from '../../utils/task/ConcreteTask';
import { RegisterActionsHandler } from './tasks/init/RegisterActionsHandler';
import { SetupKeyboard } from './tasks/init/SetupKeyboard';
import { CommHistory } from '../../types/CommHistory';

declare var leanderGMApi:any;

/** @internal */
export class LegaComm implements IComm {
    private _onReady:Signal = new Signal();

    private _initialized:boolean = false;
    private _createApiTask:ITask;

    private _model:LegaModel;
    private _response:any = {};

    constructor(){
        this._createApiTask = new CreateLeGaAPI();

        this._createApiTask.onComplete.add(() => {
            this._initialized = true;
            this._createApiTask.onComplete.removeAll();
            this._onReady.dispatch();
        }, this, -1);

        this._createApiTask.execute();

        this._model = LegaModel.getInstance();
    }

    public init(params:CommInitRequest):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();
        
        let initParams:any = {};
        for(let paramName in params){
            initParams[paramName] = (<any>params)[paramName];
        }
        initParams.responseData = {};
        initParams.savedData = {};
        initParams.currencyConfig = {};

        let sequenceTask:ITask = new SequenceTask([
            new RegisterJackpotHandler(initParams),
            new RegisterBalanceHandler(initParams),
            new RegisterErrorHandler(initParams),
            new RegisterSoundHandler(initParams),
            new RegisterInfoHandler(initParams),
            new RegisterActionsHandler(initParams),
            new RegisterDisposeHandler(),
            new InitLeGa(initParams),
            new CreateVSAPI(),
            new SetupCurrency(initParams),
            new SetupKeyboard(initParams)
        ]);

        sequenceTask.onComplete.addOnce(() => {
            for(var dataName in initParams.savedData){
                this._model.savedData.add(dataName, initParams.savedData[dataName]);
            }
            this._model.currencyConfig = initParams.currencyConfig;
            this._model.history = initParams.responseData.history;
            this._model.balance = initParams.responseData.balance.balance;
            this._model.freeBalance = initParams.responseData.balance.freeBalance;

            initParams.responseData.keyboardEnabled = this._model.keyboardEnabled;

            defer.resolve(initParams.responseData);
        });

        sequenceTask.onCanceled.addOnce( (errorCode:number) => defer.reject(errorCode) );

        if(!this._initialized){
            this._createApiTask.onComplete.add(() => {
                sequenceTask.execute();
            });
        } else {
            sequenceTask.execute();
        }

        return defer.promise;
    }

    public spin(bet:number, lines?:number, extraParams:any = null, saveData:CommSavedResponse[] = null):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();
        
        this._response = {};
        
        let params = {
            saveData: saveData,
            bet: bet,
            lines: lines,
            extraParams: extraParams,
            response: this._response
        };
        
        let sequenceTask:ITask = new SequenceTask([
            new RequestSpin(params)
        ]);
        
        sequenceTask.onComplete.addOnce( () => defer.resolve(params.response) );
        sequenceTask.onError.addOnce( (errorCode:number) => defer.reject(errorCode) );

        sequenceTask.execute();
        
        return defer.promise;
    }

    public bonus(bet:number, params:any, saveData:CommSavedResponse[] = null):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();
        
        this._response = {};
        
        let _params = {
            saveData: saveData,
            bet: bet,
            params: params,
            response: this._response
        };
        
        let sequenceTask:ITask = new SequenceTask([
            new RequestBonus(_params)
        ]);
        
        sequenceTask.onComplete.addOnce( () => defer.resolve(_params.response) );
        sequenceTask.onError.addOnce( (errorCode:number) => defer.reject(errorCode) );

        sequenceTask.execute();
        
        return defer.promise;
    }

    public gamble(bet:number, params:any, saveData:CommSavedResponse[] = null):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();
        
        this._response = {};
        
        let _params = {
            saveData: saveData,
            bet: bet,
            params: params,
            response: this._response
        };
        
        let sequenceTask:ITask = new SequenceTask([
            new RequestGamble(_params)
        ]);
        
        sequenceTask.onComplete.addOnce( () => defer.resolve(_params.response) );
        sequenceTask.onError.addOnce( (errorCode:number) => defer.reject(errorCode) );

        sequenceTask.execute();
        
        return defer.promise;
    }

    public autoplay(enabled:boolean, spins?:number, betList?:Array<number>, stopCallback?:any):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        if(enabled && ( (isNaN(spins) || spins <= 0) || ((betList == null) || (betList.length == 0)) ) ){
            defer.reject();
            return;
        }

        if(!enabled) {
            leanderGMApi.publishEvent(leanderGMApi.publications.AUTOPLAY_CANCELLED);
            this._model.autoplayActive = false;
            return;
        }

        this._response = {};

        let params = {
            spins: spins,
            betList: betList,
            stopCallback: stopCallback,
            response: this._response
        };

        let sequenceTask:ITask = new SequenceTask([
            new RequestAutoPlay(params)
        ]);

        sequenceTask.onComplete.addOnce( () => defer.resolve(params.response) );
        sequenceTask.onError.addOnce( () => defer.reject() );

        sequenceTask.execute();

        return defer.promise;
    }
    
    public close(saveData:CommSavedResponse[] = null):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();
        
        this._response = {};

        let params = {
            saveData: saveData,
            response: this._response
        };

        let sequenceTask:ITask = new SequenceTask([
            new SettleAndClosePlay(params)
        ]);

        sequenceTask.onComplete.addOnce( () => {
            if(saveData != null && saveData.length > 0){
                for(let i = 0; i < saveData.length; i++){
                    this._model.savedData.add(saveData[i].name, saveData[i].data);
                }
            }

            defer.resolve(params.response);
        });
        sequenceTask.onError.addOnce( (errorCode:number) => defer.reject(errorCode) );
        
        sequenceTask.execute();

        return defer.promise;
    }

    public save(data:CommSavedResponse[]):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        let taskList:ConcreteTask[] = [];
        for(let i = 0; i < data.length; i++){
            taskList.push(new SaveData({
                name: data[i].name,
                data: data[i].data
            }));
        }

        let sequenceTask:ITask = new SequenceTask(taskList);

        sequenceTask.onComplete.addOnce( () => {
            for(let i = 0; i < data.length; i++){
                this._model.savedData.add(data[i].name, data[i].data);
            }

            defer.resolve() 
        });
        sequenceTask.onError.addOnce( () => defer.reject() );

        sequenceTask.execute();

        return defer.promise;
    }

    public load(name:string):any {
        return this._model.savedData.get(name);
    }

    public history():CommHistory {
        return this._model.history;
    }

    public error(errorCode:number):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        if(this._model.errorCode == null){
            leanderGMApi.publishEvent(
                leanderGMApi.publications.ERROR_OCCURRED,
                {
                    data: errorCode,
                    action: this.checkNextError().then(defer.resolve),
                    error: {
                        code: errorCode
                    }
                }
            );
        } else 
        if(this._model.errorCode == errorCode){
            // Trying to throw same error, do nothing
        } else {
            // Enqueue error to throw immediately after resolve
            this._model.enqueueError(errorCode);
        }

        this._model.errorCode = errorCode;

        return defer.promise;
    }

    private checkNextError():Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        
        let nextErrorCode:number = this._model.dequeueError();
        if(nextErrorCode != null){
            this.error(nextErrorCode).then(() => {
                this._model.errorCode = null;
                
                defer.resolve();
            });
        } else {
            this._model.errorCode = null;
            
            defer.resolve();
        }

        return defer.promise;
    }

    public destroy():Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        let gameReadyForUnload:number = leanderGMApi.subscribeToEvent(leanderGMApi.publications.GAME_READY_FOR_UNLOAD,
            () => {
                leanderGMApi.unsubscribeFromEvent(gameReadyForUnload);

                defer.resolve();
            }
        );
        
        leanderGMApi.publishEvent(leanderGMApi.publications.DISPOSE_APPLICATION);

        return defer.promise;
    }

    public isGameBlocked():boolean {
        return leanderGMApi.isGUIBlocked();
    }

    public isGameIdle():boolean {
        return leanderGMApi.getGameStatusIdle();
    }

    public isReady():boolean {
        return this._initialized;
    }

    public getCurrencyConfig():object {
        return this._model.currencyConfig;
    }

    public getBalance():{balance: number, freeBalance: number} {
        return {balance: this._model.balance, freeBalance: this._model.freeBalance};
    }

    public getChannel():string {
        return leanderGMApi.getChannel();
    }

    public getLocale():string {
        return leanderGMApi.gameOptions.locale;
    }

    public getSoundEnabled():boolean {
        this._model.generalSound = leanderGMApi.getGeneralSoundActive();
        return this._model.generalSound;
    }
    
    public getMusicEnabled():boolean {
        this._model.music = leanderGMApi.getMusicActive();
        return this._model.music;
    }

    public getSFXEnabled():boolean {
        this._model.sfx = leanderGMApi.getSfxActive();
        return this._model.sfx;
    }

    public getControlPanelEnabled():boolean {
        this._model.options = leanderGMApi.gameOptions.optionsPanelStatus;
        return this._model.options;
    }

    public getAboutEnabled():boolean {
        this._model.about = leanderGMApi.gameOptions.aboutGameStatus;
        return this._model.about;
    }

    public getPaytableEnabled():boolean {
        this._model.paytable = leanderGMApi.gameOptions.payTableStatus;
        return this._model.paytable;
    }

    public getHelpEnabled():boolean {
        this._model.help = leanderGMApi.gameOptions.helpStatus;
        return this._model.help;
    }

    public getKeyboardEnabled():boolean {
        return this._model.keyboardEnabled;
    }
    
    public getJackpots():CommGameJackpot[] {
        return this._model.jackpots;
    }

    public getPlayId():number {
        let playId:number|string = leanderGMApi.getCurrentPlayId();
        return (playId == "") ? null : playId as number;
    }

    /**
     * Set preloading progress
     * @param value Value between 0 and 1.
     */
    public setProgress(value:number) {
        if(this._initialized){
            leanderGMApi.publishEvent( leanderGMApi.publications.PRELOADING_PROGRESS_UPDATED, (value * 100) );

            if(value >= 1){
                leanderGMApi.publishEvent(leanderGMApi.publications.PRELOADING_ENDED);
            }
        }
    }

    public setGameState(state:CommGameStates):Q.Promise<any> {
        let defer:Q.Deferred<any> = Q.defer();

        let stateMap = {
            [CommGameStates.CLOSE]: leanderGMApi.gameStates.CLOSE_PLAY,
            [CommGameStates.IDLE]: leanderGMApi.gameStates.IDLE,
            [CommGameStates.INITIALIZE]: leanderGMApi.gameStates.INITIALIZE,
            [CommGameStates.OPEN]: leanderGMApi.gameStates.OPEN_PLAY,
            [CommGameStates.SETTLE]: leanderGMApi.gameStates.SETTLE_PLAY,
        };

        if(state == CommGameStates.READY){
            // Can only be called once. Will let the LeanderGMApi know that the game is initialized and ready to operate.
            
            // If game had responseHistory, set the state on Open, otherwise, set it on Idle.
            if(this._model.history != null){
                this.setGameState(CommGameStates.OPEN);
            } else {
                this.setGameState(CommGameStates.IDLE);
            }

            // Call init notification task
            let task:NotifyInitialized = new NotifyInitialized();
            task.onComplete.addOnce( () => {
                defer.resolve({
                    freePlays: this._model.freePlays,
                    history: this._model.history && this._model.history.history ? this._model.history.history : null
                });
            });
            task.execute();

            return defer.promise;
        }

        if(state == CommGameStates.RESULTS){
            leanderGMApi.publishEvent(leanderGMApi.publications.GAME_RESULTS_SHOWN);
            
            defer.resolve();

            return defer.promise;
        }

        if(state == CommGameStates.INPLAY){
            leanderGMApi.publishEvent(leanderGMApi.publications.REELS_STOPPED);
            
            defer.resolve();

            return defer.promise;
        }

        if(stateMap[state] == null){
            defer.reject();

            return defer.promise;
        }

        leanderGMApi.publishEvent(leanderGMApi.publications.GAME_STATUS_CHANGED, stateMap[state]);

        defer.resolve();

        return defer.promise;
    }

    public setBalance(balance:number, freeBalance:number):void {
        leanderGMApi.publishEvent(leanderGMApi.publications.BALANCE_UPDATED_GAME, { balance: balance, free_balance: freeBalance });

        this._model.balance = balance;
        this._model.freeBalance = freeBalance;
    }

    public setBet(value:number):void {
        leanderGMApi.publishEvent(leanderGMApi.publications.TOTALBET_UPDATED, value);
    }

    public setWinnings(value:number):void {
        leanderGMApi.publishEvent(leanderGMApi.publications.TOTALWIN_UPDATED, value);
    }

    public setSoundEnabled(value:boolean):void {
        this._model.generalSound = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.GENERAL_SOUND_STATUS_CHANGED_GAME, value);
    }

    public setMusicEnabled(value:boolean):void {
        this._model.music = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.MUSIC_STATUS_CHANGED_GAME, value);
    }
    
    public setSFXEnabled(value:boolean):void {
        this._model.sfx = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.SOUND_EFFECTS_STATUS_CHANGED_GAME, value);
    }

    public setControlPanelEnabled(value:boolean):void {
        this._model.options = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.CONTROL_PANEL_STATUS_CHANGED_GAME, value);
    }

    public setAboutEnabled(value:boolean):void {
        this._model.about = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.ABOUT_GAME_STATUS_CHANGED_GAME, value);
    }

    public setPaytableEnabled(value:boolean):void {
        this._model.paytable = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.PAY_TABLE_STATUS_CHANGED_GAME, value);
    }

    public setFreePlaysMode(value:CommGameFreePlaysModes):void {
        switch(value){
            case CommGameFreePlaysModes.CUSTOM:
                leanderGMApi.publishEvent(leanderGMApi.publications.PROMOTION_WRAPPER_DISABLED);
                leanderGMApi.deactivateWrapper();
                leanderGMApi.gamePromotionWrapperDisabledOnGame = true;
            break;
            case CommGameFreePlaysModes.NATIVE:
                leanderGMApi.activateWrapper();
                leanderGMApi.gamePromotionWrapperDisabledOnGame = false;
            break;
        }
    }

    public get onReady():Signal {
        return this._onReady;
    }

    public setHelpEnabled(value:boolean):void {
        this._model.help = value;
        leanderGMApi.publishEvent(leanderGMApi.publications.HELP_STATUS_CHANGED_GAME, value);
    }
}