import * as Q from 'q';
import { IComm } from "../IComm";
import { CommInitRequest } from "../../types/CommInitRequest";
import { CommGameFreePlaysModes } from "../../types/CommGameFreePlaysModes";
import { CommGameStates } from "../../types/CommGameStates";
import { Signal } from "signals";
import { CommInitResponse } from '../../types/CommInitResponse';
import { CommResponseStates } from '../../types/CommResponseStates';
import { CommGameJackpot } from '../../types/CommGameJackpot';
import { CommSavedResponse } from '../../types/CommSavedResponse';

/** @internal */
export class DummyComm implements IComm {
    onReady:Signal = new Signal();

    private _state:CommGameStates = CommGameStates.IDLE;
    private _music:boolean = true;
    private _sfx:boolean = true;
    private _balance:number = 1000;
    private _freeBalance:number = 0;

    constructor() {
        this.onReady.dispatch();
    }

    init(params:CommInitRequest):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        let extraParams = {};
        for(let i = 0; i < params.extraParams.length; i++){
            (<any>extraParams)[params.extraParams[i]] = "";
        }

        let response:CommInitResponse = {
            autoplay: {
                value: 1,
                list: [1]
            },
            balance: {
                balance: this._balance,
                freeBalance: this._freeBalance
            },
            bet: {
                value: 1,
                list: [1]
            },
            extraParams: extraParams,
            freePlays: null,
            jackpots: [],
            gameRules: () => {},
            history: null,
            lines: [[1,2,3]],
            lobby: () => {},
            keyboardEnabled: true,
            logoEnabled: true,
            customConfig: {
                customAssets: "default",
                customIndex: -1
            },
            rtp: {
                game: 100,
                jackpot: 0,
                upper: 0
            },
            type: CommResponseStates.INIT
        }

        defer.resolve(<any>response);

        return defer.promise;
    }

    spin(bet:number, lines:number, extraParams:any, saveData:CommSavedResponse[]):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }

    bonus(bet:number, params?:any):Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }

    gamble(bet:number, params?:any):Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }
    
    autoplay(enabled:boolean, spins?:number, betList?:Array<number>, stopCallback?:any):Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }

    close(saveData:CommSavedResponse[] = null):Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }

    save(data:CommSavedResponse[]):Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }

    load(name:string):any { 
        return null;
    }

    history():any { 
        return null;
    }

    error(errorCode:number):Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        let response:any = {};

        defer.resolve(response);

        return defer.promise;
    }

    destroy():Q.Promise<void> { 
        let defer:Q.Deferred<void> = Q.defer<void>();

        defer.resolve();

        return defer.promise;
    }


    isGameBlocked():boolean { 
        return false;
    }

    isGameIdle():boolean { 
        return this._state == CommGameStates.IDLE;
    }

    isReady():boolean { 
        return true;
    }


    getBalance():{balance:number, freeBalance:number} {
        return {balance: this._balance, freeBalance: this._freeBalance};
    }

    getChannel():string {
        return "mobile";
    }

    getLocale():string {
        return "en";
    }

    getSoundEnabled():boolean {
        return this._music && this._sfx;
    }
    
    getMusicEnabled():boolean {
        return this._music;
    }

    getSFXEnabled():boolean {
        return this._sfx;
    }

    getControlPanelEnabled():boolean {
        return false;
    }

    getAboutEnabled():boolean {
        return false;
    }

    getPaytableEnabled():boolean {
        return false;
    }

    getHelpEnabled():boolean {
        return false;
    }

    getKeyboardEnabled():boolean {
        return false;
    }
    
    getCurrencyConfig():object {
        return null;
    }
    
    getJackpots():CommGameJackpot[] {
        return [];
    }

    getPlayId():number {
        return 0;
    }
    
    setProgress(value:number):void {

    }

    setGameState(state:CommGameStates):Q.Promise<any> {
        this._state = state;
        
        return null;
    }

    setBalance(balance:number, freeBalance:number):void {
        this._balance = balance;
        this._freeBalance = freeBalance;
    }

    setBet(value:number):void {

    }

    setWinnings(value:number):void {

    }

    setSoundEnabled(value:boolean):void {

    }

    setMusicEnabled(value:boolean):void {
        this._music = value;
    }

    setSFXEnabled(value:boolean):void {
        this._sfx = value;
    }

    setControlPanelEnabled(value:boolean):void {

    }

    setAboutEnabled(value:boolean):void {

    }

    setPaytableEnabled(value:boolean):void {

    }

    setFreePlaysMode(value:CommGameFreePlaysModes):void {

    }

    setHelpEnabled(value:boolean):void {

    }
    

}