import { ArrowModel } from "./ArrowModel";
import { CommSpinWinnings, CommSpinWinningsLine } from "../../types/CommSpinResponse";

/** @internal */
export class ArrowUtils {
    static toCurrency(value:(number|string|null)):number {
        if(value == null) return 0;
        let divisor:number = ArrowModel.getInstance().currencyDivisor;
        let ret:number = ( (typeof(value) == "string") ? (parseFloat(<string>value) / divisor) : (<number>value / divisor) );
        
        return Math.round(ret * divisor) / divisor;
    }
    
    static toCents(value:number):number {
        let divisor:number = ArrowModel.getInstance().currencyDivisor;
        let ret:number = ( (typeof(value) == "string") ? (parseFloat(<string>value) * divisor) : (<number>value * divisor) );
        
        return Math.round(ret);
    }

    static isNumerableProp(propName:string, propPrefix:string):boolean {
        return ( (propName.indexOf(propPrefix) >= 0) && (propName.replace(propPrefix, "") == parseInt(propName.replace(propPrefix, "")).toString()) );
    }
    
    static getPropIndex(propName:string, propPrefix:string):number {
        return propName.indexOf(propPrefix) >= 0 && !isNaN(parseInt(propName.replace(propPrefix, ""))) ? parseInt(propName.replace(propPrefix, "")) : -1;
    }

    static getReel(response:any):string[] {
        if(!response.hasOwnProperty("sp")) return [];
        let reelLayout:string[][] = [];

        for(let prop in response){
            if(ArrowUtils.isNumerableProp(prop, "sr")){
                let reelIndex:number = ArrowUtils.getPropIndex(prop, "sr");
                reelLayout[reelIndex] = response[prop].split(",");
            }
        }

        let ret:string[] = [];
        for(let r = 0; r < reelLayout[0].length; r++){
            for(let c = 0; c < reelLayout.length; c++){
                ret.push(reelLayout[c][r]);
            }
        }

        return ret;
    }

    static getWinnings(response:any):CommSpinWinnings {
        let ret:CommSpinWinnings = {
            creditsWon: (response.hasOwnProperty("wt") ? ArrowUtils.toCurrency(response.wt) : 0),
            lines: ArrowUtils.getLines(response)
        }

        return ret;
    }
    
    static getLines(response:any):CommSpinWinningsLine[] {
        if(!response.hasOwnProperty("nwl")) return [];
        let ret:CommSpinWinningsLine[] = new Array<CommSpinWinningsLine>(parseFloat(response.nwl));

        for(let prop in response){
            if(ArrowUtils.isNumerableProp(prop, "wl")){
                let lineIndex:number = ArrowUtils.getPropIndex(prop, "wl");
                let lineArray:any[] = response[prop].split(",");
                ret[lineIndex] = {
                    line: parseInt(lineArray[0]) + 1,
                    count: parseInt(lineArray[1]),
                    symbol: lineArray[2],
                    creditsWon: ArrowUtils.toCurrency(lineArray[3]),
                    multiplier: parseFloat(response.mul),
                    positions: [],
                    extraData: null
                }
            }
        }

        return ret;
    }
    
    static parseBonus(response:any):any[] {
        // For Arrow's, bonus are updated per request, so if "ns" property is different to "0", anything that doesn't belong
        // to a regular spin should be considered bonus info
        let initParams:string[] = ["maxbet", "minbet", "id", "_maxbet_ic", "_minbet_ic", "gp_cur", "chipvaluedivisor", "b", "l", "chipvalues", "balance", "_fsb_id", "_fsb_bps", "_fsb_maxl", "_fsb_award", "_fsb_spins"];
        let spinParams:string[] = ["t", "gp_a", "_bal_ic","_pg_ic","wt","sp","nwl","jpw"];
        let countableSpinParams:string[] = ["sr", "wl"];
        let ret:any[] = [];
        let obj:any = {};
        let isFS:boolean = (response.t != null) && (response.t.toLowerCase() == "freespin" || response.t.toLowerCase() == "freespins" || response.t.toLowerCase() == "free_spin" || response.t.toLowerCase() == "free_spins");
        for(let param in response){
            let skip:boolean = false;
            if(!isFS){
                for(let i = 0; i < countableSpinParams.length; i++){
                    if( (param.indexOf(countableSpinParams[i]) >= 0) && ArrowUtils.isNumerableProp(param, countableSpinParams[i]) ){
                        skip = true;
                        break;
                    }
                }
            }
            if(!skip){
                let checkArray:string[] = isFS ? initParams : initParams.concat(spinParams);
                if(checkArray.indexOf(param) < 0){
                    obj[param] = response[param];
                }
            }
        }

        ret.push(obj);

        return ret;
    }
}