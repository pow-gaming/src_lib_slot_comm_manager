import * as Q from 'q';
import { CommGameFreePlays } from '../../types/CommGameFreePlays';
import Dictionary from '../../utils/Dictionary';
import { CommGameJackpot } from '../../types/CommGameJackpot';
import { Signal } from 'signals';
import { ArrowUtils } from './ArrowUtils';

/** @internal */
export class ArrowModel {
    private static instance:ArrowModel;

    private _endPointURL: string;
    private _timeout: number;
    private _requestId: number;
    private _userId: string;
    private _sessionId: string;
    private _gameId: string;
    private _tournamentId:string;
    private _freePlaysId:string;
    private _lines: number;
    private _autoplayActive: boolean;
    private _currencyDivisor:number;
    private _playingForReal:boolean;
    private _freePlays:CommGameFreePlays;
    private _currencySign:string;
    private _jackpots: Dictionary<string, CommGameJackpot>;

    private _balance:number;
    
    onJackpotUpdate:Signal;
    onBalanceUpdate:Signal;
    onError:Signal;

    private constructor() {
        this._endPointURL = "";
        this._timeout = 0;
        this._requestId = 0;
        this._userId = "";
        this._sessionId = "";
        this._gameId = "";
        this._tournamentId = "";
        this._freePlaysId = "";
        this._playingForReal = false;
        this._lines = 0;
        this._autoplayActive = false;
        this._currencyDivisor = 100;
        this._freePlays = null;
        this._currencySign = "";
        this._jackpots = new Dictionary<string, CommGameJackpot>((k => k));
        this.onJackpotUpdate = new Signal();
        this.onBalanceUpdate = new Signal();
        this.onError = new Signal();
    }
    
    static getInstance():ArrowModel {
        if(!ArrowModel.instance){
            ArrowModel.instance = new ArrowModel();
        }
        
        return ArrowModel.instance;
    }

    public sendRequest(data:any): Q.Promise<void> {
        var defer:Q.Deferred<void> = Q.defer<void>();

        $.ajax({
            method: 'get',
            url: this._endPointURL,
            data: $.param(data),
            cache: false,
            timeout: this._timeout,
            async: true

        }).done(defer.resolve.bind(this))
          .fail(defer.reject.bind(this));

        return defer.promise;
    }

    public parseQuery(data:string): any {        
        let ret:any = {};
        let resultString:string[] = data.split("&");
        for (let i = 0; i < resultString.length; i++) {
            let objArray:string[] = (resultString[i]).split("=");
            if (objArray[0] != null) {
                let key:string = objArray[0];

                if (objArray[1] != null) {
                    ret[key] = unescape(objArray[1]);
                }
            }
        }

        return ret;
    }
    
    parseJackpots(data:any): void {
        this._jackpots.clear();
        
        let jackpotList:string[] = Object.keys(data).filter( id => id.indexOf("_pg_ic") >= 0);

        for(let i = 0; i < jackpotList.length; i++){
            let jackpotId:number = (jackpotList[i] == "_pg_ic") ? 0 : parseInt(jackpotList[i].replace("_pg_ic", ""));
            let jackpotValue:number = ArrowUtils.toCurrency(data[jackpotList[i]]);
            let jackpotPrize:number = ArrowUtils.toCurrency(data[jackpotList[i].replace("_pg_ic", "jpw")]);
            
            let jackpot:CommGameJackpot = {
                active: true,
                awarded: (jackpotPrize > 0),
                contribution: 0,
                currency: this._currencySign,
                details: "",
                eligibleUsers: 0,
                id: jackpotId,
                isDeferred: false,
                isProgressive: true,
                name: "Jackpot_" + jackpotId.toString(),
                prize: (jackpotPrize > 0) ? jackpotPrize : jackpotValue,
                seeding: 0
            };

            this._jackpots.setValue(jackpot.id.toString(), jackpot);
        }

        this.onJackpotUpdate.dispatch();
    }

    set jackpots(value:CommGameJackpot[]) {    
        for(let i = 0; i < value.length; i++){
            this._jackpots.setValue(value[i].id.toString(), value[i]);
        }

        this.onJackpotUpdate.dispatch();
    }

    get jackpots():CommGameJackpot[] {
        let ret:CommGameJackpot[] = this._jackpots.values();
        ret = ret.sort((a, b) => ((a.id < b.id) ? -1 : 1));
        return ret;
    }
    
    parseFreePlays(data:any): void {
        if(data == null) return;
        if(JSON.stringify(data) == "{}") return;
        if(isNaN(parseFloat(data._fsb_id)) || parseFloat(data._fsb_id) <= 0) return;
       
        let maxLines:number = (this.lines > 0 ? this.lines : null);

        this._freePlays = {
            bet: ArrowUtils.toCurrency(data._fsb_bps),
            lines: (data._fsb_maxl.toUpperCase() == "T") ? maxLines : data.promotionLines,
            totalWin: ArrowUtils.toCurrency(data._fsb_award),
            playsDone: 0,
            playsLeft: parseFloat(data._fsb_spins),
            playsTotal: parseFloat(data._fsb_spins)
        };
    }

    clearFreePlays():void {
        this._freePlays = null;
    }

    get freePlays():CommGameFreePlays {
        return this._freePlays;
    }
    
    public get endPointURL(): string {
        return this._endPointURL;
    }
    
    public set endPointURL(value: string) {
        this._endPointURL = value;
    }

    public get timeout(): number {
        return this._timeout;
    }

    public set timeout(value: number) {
        this._timeout = value;
    }

    public get requestId(): number {
        return ++this._requestId;
    }

    public get userId(): string {
        return this._userId;
    }
    
    public set userId(value: string) {
        this._userId = value;
    }
    
    public get sessionId(): string {
        return this._sessionId;
    }

    public set sessionId(value: string) {
        this._sessionId = value;
    }

    public get gameId(): string {
        return this._gameId;
    }

    public set gameId(value: string) {
        this._gameId = value;
    }

    public get tournamentId(): string {
        return this._tournamentId;
    }

    public set tournamentId(value: string) {
        this._tournamentId = value;
    }

    public get freePlaysId(): string {
        return this._freePlaysId;
    }

    public set freePlaysId(value: string) {
        this._freePlaysId = value;
    }

    public set autoplayActive(value:boolean) {
        this._autoplayActive = value;
    }

    public get autoplayActive():boolean {
        return this._autoplayActive;
    }

    public set currencySign(value:string) {
        this._currencySign = value;
    }

    public get currencySign():string {
        return this._currencySign;
    }

    public get lines(): number {
        return this._lines;
    }

    public set lines(value: number) {
        this._lines = value;
    }

    public get currencyDivisor():number {
        return this._currencyDivisor;
    }

    public set currencyDivisor(value:number) {
        this._currencyDivisor = value;
    }

    public get balance():number {
        return this._balance;
    }

    public set balance(value:number) {
        this._balance = value;
        this.onBalanceUpdate.dispatch();
    }

    public get playingForReal():boolean {
        return this._playingForReal;
    }

    public set playingForReal(value:boolean) {
        this._playingForReal = value;
    }

    public setError():void {
        this.onError.dispatch({code: 999, message: "Error"});
    }
}