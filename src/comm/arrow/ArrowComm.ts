import * as Q from 'q';
import { IComm } from "../IComm";
import { Signal } from "signals";
import { CommInitRequest, CommGameJackpot, CommGameStates, CommGameFreePlaysModes } from "../..";
import { CommSavedResponse } from "../../types/CommSavedResponse";
import { ITask } from '../../utils/task/ITask';
import { SequenceTask } from '../../utils/task/SequenceTask';
import { InitArrow } from './tasks/init/InitArrow';
import { ArrowModel } from './ArrowModel';
import { URLVars } from './../../utils/URLVars';
import { GetReels } from './tasks/init/GetReels';
import { RegisterBalanceHandler } from './tasks/init/RegisterBalanceHandler';
import { CurrencyConfig } from '../../utils/types/CurrencyConfig';
import { RequestSpin } from './tasks/spin/RequestSpin';
import { RestoreData } from './tasks/init/RestoreData';
import { RegisterErrorHandler } from './tasks/init/RegisterErrorHandler';
import { RequestBonus } from './tasks/bonus/RequestBonus';
import { RegisterJackpotHandler } from './tasks/init/RegisterJackpotHandler';

/** @internal */
export class ArrowComm implements IComm {
    private _model:ArrowModel = ArrowModel.getInstance();
    private _response:any = {};

    onReady:Signal;

    constructor() {
        this._model.endPointURL = URLVars.getQueryVariable("serverAddress") || "http://stage.gtbets.eu:8082/casino/gameservlet";
        this._model.timeout = 15000;
    }

    public init(params:CommInitRequest):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();
        
        let initParams:any = {};
        for(let paramName in params){
            initParams[paramName] = (<any>params)[paramName];
        }
        initParams.rawInitData = {};
        initParams.responseData = {};

        let sequenceTask:ITask = new SequenceTask([
            new RegisterBalanceHandler(initParams),
            new RegisterJackpotHandler(initParams),
            new RegisterErrorHandler(initParams),
            new InitArrow(initParams),
            new RestoreData(initParams),
            new GetReels(initParams)
        ]);

        sequenceTask.onComplete.addOnce(() => {
            initParams.responseData.extraParams.reels = initParams.reels;
            /*
            for(var dataName in initParams.savedData){
                this._model.savedData.add(dataName, initParams.savedData[dataName]);
            }
            this._model.currencyConfig = initParams.currencyConfig;
            this._model.history = initParams.responseData.history;
            this._model.balance = initParams.responseData.balance.balance;
            this._model.freeBalance = initParams.responseData.balance.freeBalance;
            */
            defer.resolve(initParams.responseData);
        });

        sequenceTask.onError.addOnce( () => defer.reject() );
        sequenceTask.onCanceled.addOnce( () => defer.reject() );
        sequenceTask.execute();

        return defer.promise;
    }

    public spin(bet:number, lines?:number, extraParams?:any, saveData?:CommSavedResponse[]):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        this._response = {};
        
        let params = {
            bet: bet,
            lines: lines,
            extraParams: extraParams,
            response: this._response
        };
        
        let sequenceTask:ITask = new SequenceTask([
            new RequestSpin(params)
        ]);
        
        sequenceTask.onComplete.addOnce( () => defer.resolve(params.response) );
        sequenceTask.onError.addOnce( () => defer.reject(params.response) );

        sequenceTask.execute();
        
        return defer.promise;
    }

    public bonus(bet:number, params?:any):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        this._response = {};
        
        let _params = {
            params: params,
            response: this._response
        };
        
        let sequenceTask:ITask = new SequenceTask([
            new RequestBonus(_params)
        ]);
        
        sequenceTask.onComplete.addOnce( () => defer.resolve(_params.response) );
        sequenceTask.onError.addOnce( () => defer.reject(_params.response) );

        sequenceTask.execute();
        return defer.promise;
    }

    public gamble(bet:number, params?:any):Q.Promise<void> {
        return this.bonus(bet, params);
    }

    public autoplay(enabled:boolean, spins?:number, betList?:Array<number>, stopCallback?:any):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        if(enabled && ( (isNaN(spins) || spins <= 0) || ((betList == null) || (betList.length == 0)) ) ){
            defer.reject();
            return;
        }

        if(!enabled) {
            this._model.autoplayActive = false;
            return;
        }

        this._model.autoplayActive = enabled;

        defer.resolve.call(this, spins);

        return defer.promise;
    }

    public close(saveData:CommSavedResponse[]):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        defer.resolve();

        return defer.promise;
    }

    public save(data:CommSavedResponse[]):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        return defer.promise;
    }

    public load(name:string):any {

    }

    public history():any {

    }

    public error(errorCode:number):Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        this._model.setError();

        defer.resolve();

        return defer.promise;
    }

    public destroy():Q.Promise<void> {
        let defer:Q.Deferred<void> = Q.defer<void>();

        return defer.promise;
    }


    public isGameBlocked():boolean {
        return false;
    }

    public isGameIdle():boolean {
        return true;
    }

    public isReady():boolean {
        return true;
    }

    public getBalance():{balance:number, freeBalance:number} {
        return {
            balance: this._model.balance,
            freeBalance: 0
        };
    }

    public getChannel():string {
        return "mobile";
    }

    public getLocale():string {
        return URLVars.getQueryVariable("lang") || "en";
    }

    public getSoundEnabled():boolean {
        return true;
    }

    public getMusicEnabled():boolean {
        return true;
    }

    public getSFXEnabled():boolean {
        return true;
    }

    public getControlPanelEnabled():boolean {
        return false;
    }

    public getAboutEnabled():boolean {
        return false;
    }
    
    public getPaytableEnabled():boolean {
        return false;
    }

    public getHelpEnabled():boolean {
        return false;
    }

    public getKeyboardEnabled():boolean {
        return true;
    }

    public getCurrencyConfig():object {
        let ret:CurrencyConfig = {
            mask: this._model.currencySign + "0,0.",
            config: {
                delimiters: {
                    thousands: ",",
                    decimal: "."
                },
                currency: {
                    symbol: this._model.currencySign
                }
            }
        }

        for(let i = 0; i < Math.log10(this._model.currencyDivisor); i++){
            ret.mask += "0";
        }

        return ret;
    }

    public getJackpots():CommGameJackpot[] {
        return [];
    }

    public getPlayId():number {
        return 0;
    }

    
    public setProgress(value:number):void {

    }

    public setGameState(state:CommGameStates):Q.Promise<any> {
        return null;
    }

    public setBalance(balance:number, freeBalance:number):void {

    }

    public setBet(value:number):void {

    }

    public setWinnings(value:number):void {

    }

    public setSoundEnabled(value:boolean):void {

    }

    public setMusicEnabled(value:boolean):void {

    }

    public setSFXEnabled(value:boolean):void {

    }

    public setControlPanelEnabled(value:boolean):void {

    }

    public setAboutEnabled(value:boolean):void {

    }

    public setPaytableEnabled(value:boolean):void {

    }

    public setFreePlaysMode(value:CommGameFreePlaysModes):void {

    }

    public setHelpEnabled(value:boolean):void {

    }

}