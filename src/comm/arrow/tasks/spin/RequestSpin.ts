import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ArrowModel } from "../../ArrowModel";
import { CommSpinResponse } from "../../../../types/CommSpinResponse";
import { CommResponseStates } from "../../../../types/CommResponseStates";
import { ArrowUtils } from "../../ArrowUtils";
import { CommErrorResponse } from "../../../../types/CommErrorResponse";

/** @internal */
export class RequestSpin extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();

    execute(params: any = null): void {
        this._model.sendRequest(this.serialize())
            .then(e => {
                let response:any = this._model.parseQuery(<any>e);
                if(response.gp_e != null){
                    let error:CommErrorResponse = {
                        code: response.gp_e,
                        isCritical: true,
                        message: "",
                        userCancelled: false,
                        userConfirmed: false
                    };
                    this.params.responseData = error;

                    this.sendError();
                } else {
                    this.params.response = this.deserialize(response);
                
                    this.sendComplete();
                }
            })
            .fail(e => {
                let error:CommErrorResponse = {
                    code: 0,
                    isCritical: true,
                    message: "",
                    userCancelled: false,
                    userConfirmed: false
                };
                this.params.responseData = error;
            
                this.sendError();
            });
    }

    private serialize():any {
        let ret:any = {};
        
        ret.t = "SPIN";
        ret.l = this.params.lines;
        ret.b = ArrowUtils.toCents(this.params.bet / this.params.lines);
        ret.gp_gn = this._model.gameId;
        ret.gp_a = this._model.userId;
        ret.gp_ci = this._model.sessionId;
        ret.gp_rm = this._model.playingForReal ? "T" : "F";
        ret.gp_tid = this._model.tournamentId;
        ret.gp_fsbid = this._model.freePlaysId;
        ret.id = this._model.requestId;

        return ret;
    }

    private deserialize(response:any): any {
        this._model.balance = ArrowUtils.toCurrency(response._bal_ic);
        this._model.parseFreePlays(response);
        this._model.parseJackpots(response);

        let ret:CommSpinResponse = {
            bonus: ArrowUtils.parseBonus(response),
            creditsWon: ArrowUtils.toCurrency(response.wt),
            freePlays: this._model.freePlays,
            type: CommResponseStates.SPIN,
            winnings: ArrowUtils.getWinnings(response),
            reel: ArrowUtils.getReel(response),
            reelKey: "",
            getBonus: (name:string): any => {
                return null;
            },
            getProp: (name:string): any => {
                return null;
            }
        }

        return ret;
    }
}