import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { CommErrorResponse } from "../../../../types/CommErrorResponse";
import { ArrowModel } from "../../ArrowModel";

/** @internal */
export class RegisterErrorHandler extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();

    execute(params:any = null):void {        
        this._model.onError.add(this.handleErrorResponse, this);

        this.sendComplete();
    }
    
    private handleErrorResponse(data: any): void {
        let error:CommErrorResponse = {
            code: data.code,
            userConfirmed: true,
            userCancelled: false,
            isCritical: true,
            message: data.message
        };

        this.params.onErrorHandled(error);
    }
}