import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ArrowModel } from "../../ArrowModel";
import { ArrowUtils } from "../../ArrowUtils";
import Dictionary from "../../../../utils/Dictionary";
import { CommSpinResponse } from "../../../../types/CommSpinResponse";
import { CommHistory } from "../../../../types/CommHistory";
import { CommResponseStates } from "../../../../types/CommResponseStates";

/** @internal */
export class RestoreData extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();
    private _props:Dictionary<string, string> = new Dictionary<string, string>();

    execute(params: any = null): void {
        let initData:any = this.params.rawInitData;

        if(initData.hasOwnProperty("cs")){
            // Create dictionary for optimization purposes
            for(let prop in initData){
                this._props.setValue(prop, initData[prop]);
            }

            // Start restoration
            this.restoreReelLayout();

            this.restoreSpinResponse();
        }

        this.sendComplete();
    }

    private restoreReelLayout():void {
        if(!this._props.containsKey("sp")) return;
        let numReels:number = this._props.getValue("sp").split(",").length;
        let reelLayout:string[][] = [];

        for(let i = 0; i < numReels; i++){
            let reel:string[] = this._props.getValue("sr" + i).split(",");
            reelLayout.push(reel);
        }

        this.params.responseData.extraParams.push({"noPrizeReelLayout": reelLayout});
    }

    private restoreSpinResponse():void {
        let history:CommHistory = {
            history: []
        };

        let spinResponse:CommSpinResponse = {
            creditsWon: (this._props.containsKey("wt")) ? ArrowUtils.toCurrency(this._props.getValue("wt")) : 0,
            reel: ArrowUtils.getReel(this.params.rawInitData),
            reelKey: "",
            type: CommResponseStates.SPIN,
            winnings: ArrowUtils.getWinnings(this.params.rawInitData),
            freePlays: this._model.freePlays,
            bonus: ArrowUtils.parseBonus(this.params.rawInitData),
            getBonus: (name:string):any => null,
            getProp: (name:string):any => null
        };

        history.history.push(spinResponse);

        this.params.responseData.history = history;
    }
}