import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ArrowModel } from "../../ArrowModel";

/** @internal */
export class RegisterBalanceHandler extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();
    
    execute(params:any = null):void {        
        this._model.onBalanceUpdate.add(this.handleBalanceUpdate, this);

        this.sendComplete();
    }
    
    private handleBalanceUpdate(data: any): void {
        let balance:number = this._model.balance;

        this.params.onBalanceUpdate({
            balance: balance,
            freeBalance: 0
        });
    }
}