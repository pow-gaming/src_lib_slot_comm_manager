import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ArrowModel } from "../../ArrowModel";
import { ArrowUtils } from "../../ArrowUtils";

/** @internal */
export class GetReels extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();

    execute(params: any = null): void {
        this._model.sendRequest(this.serialize())
            .then(e => {
                this.params.reels = this.deserialize(e);
                
                this.sendComplete();
            })
            .fail(e => {
                this.params.responseData = e;
                
                this.sendCanceled();
            });
    }

    private serialize():any {
        let ret:any = {};
        
        ret.t = "REELS";
        ret.gp_gn = this._model.gameId;
        ret.gp_a = this._model.userId;
        ret.gp_ci = this._model.sessionId;
        ret.gp_rm = this._model.playingForReal ? "T" : "F";
        ret.gp_tid = this._model.tournamentId;
        ret.gp_fsbid = this._model.freePlaysId;
        ret.id = this._model.requestId;

        return ret;
    }

    private deserialize(data:any): any {
        let response:any = this._model.parseQuery(data);
        
        this._model.balance = ArrowUtils.toCurrency(response._bal_ic);

        // Main Reels are on the form "rX"
        // FS Reels are on the form "rfX"
        let ret:any = {
            main: [],
            fs: []
        };
        
        for(let name in response){
            if(ArrowUtils.isNumerableProp(name, "r")){
                let reelIndex:number = ArrowUtils.getPropIndex(name, "r");
                ret.main[reelIndex] = response[name].split(",");
            } else 
            if(ArrowUtils.isNumerableProp(name, "rf")){
                let reelIndex:number = ArrowUtils.getPropIndex(name, "rf");
                ret.fs[reelIndex] = response[name].split(",");
            }
        }

        return ret;
    }
}