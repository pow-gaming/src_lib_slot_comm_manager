import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { CommErrorResponse } from "../../../../types/CommErrorResponse";
import { ArrowModel } from "../../ArrowModel";

/** @internal */
export class RegisterJackpotHandler extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();

    execute(params:any = null):void {
        // Arrow's specifications: jackpot gets updated every 5 seconds
        setInterval(this.updateJackpots.bind(this), 5000);

        this._model.onJackpotUpdate.add(this.handleJackpotUpdate, this);

        this.sendComplete();
    }

    private updateJackpots():void {
        this._model.sendRequest(this.serialize())
            .then(e => {
                let response:any = this._model.parseQuery(<any>e);

                this._model.parseJackpots(response);
            })
            .fail(e => {
                this.params.responseData = e;
            });
    }

    private serialize():any {
        let ret:any = {};
        
        ret.t = "progressive";
        ret.gp_gn = this._model.gameId;
        ret.gp_a = this._model.userId;
        ret.gp_ci = this._model.sessionId;
        ret.gp_rm = this._model.playingForReal ? "T" : "F";
        ret.gp_tid = this._model.tournamentId;
        ret.gp_fsbid = this._model.freePlaysId;
        ret.id = this._model.requestId;

        return ret;
    }

    private handleJackpotUpdate(data: any): void {
        this.params.onJackpotUpdate(this._model.jackpots);
    }
}