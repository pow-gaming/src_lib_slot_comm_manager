import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ArrowModel } from "../../ArrowModel";
import { URLVars } from "./../../../../utils/URLVars";
import { CommInitResponse } from "../../../../types/CommInitResponse";
import { CommResponseStates } from "../../../..";
import { ArrowUtils } from "../../ArrowUtils";
import { CommErrorResponse } from "../../../../types/CommErrorResponse";

/** @internal */
export class InitArrow extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();

    execute(params: any = null): void {
        this._model.sendRequest(this.serialize())
            .then(e => {
                let response:any = this._model.parseQuery(<any>e);
                if(response.gp_e != null){
                    let error:CommErrorResponse = {
                        code: response.gp_e,
                        isCritical: true,
                        message: "",
                        userCancelled: false,
                        userConfirmed: false
                    };
                    this.params.responseData = error;

                    this.sendError();
                } else {
                    this.params.responseData = this.deserialize(response);
                    
                    this.sendComplete();
                }
            })
            .fail(e => {
                let error:CommErrorResponse = {
                    code: 0,
                    isCritical: true,
                    message: "",
                    userCancelled: false,
                    userConfirmed: false
                };
                this.params.responseData = error;

                this.sendError();
            });
    }

    private serialize():any {
        let ret:any = {};
        
        ret.t = "gp_l";
        ret.gp_a = URLVars.getQueryVariable("userId");
        ret.gp_gn = URLVars.getQueryVariable("gameId");
        ret.gp_ci = URLVars.getQueryVariable("sessionId");
        ret.id = this._model.requestId;
        ret.gp_rm = URLVars.getQueryVariable("realMoney") || "T";
        ret.gp_tid = URLVars.getQueryVariable("tournamentId") || "0";
        ret.gp_fsbid = URLVars.getQueryVariable("fsbid") || "0";

        this._model.userId = ret.gp_a;
        this._model.gameId = ret.gp_gn;
        this._model.playingForReal = (ret.gp_rm == "T");
        this._model.tournamentId = ret.gp_tid;
        this._model.freePlaysId = ret.gp_fsbid;

        return ret;
    }

    private deserialize(response:any): CommInitResponse {
        this.params.rawInitData = response;

        this._model.sessionId = URLVars.getQueryVariable("sessionId");
        this._model.currencySign = decodeURIComponent(response.gp_cur);
        this._model.currencyDivisor = parseFloat(response.chipvaluedivisor);
        this._model.parseFreePlays(response);
        this._model.parseJackpots(response);

        let responseData:CommInitResponse = {
            balance: {
                balance: ArrowUtils.toCurrency(response._bal_ic),
                freeBalance: 0
            },
            autoplay: {
                value: 5,
                list: [5,10,15,20,25,30,35,40,45,50]
            },
            history: null,
            bet: {
                value: ArrowUtils.toCurrency(response.b),
                list: response.chipvalues.split(",").map( (value:string) => ArrowUtils.toCurrency(value) )
            },
            rtp: {
                game: 0,
                jackpot: 0,
                upper: 0
            },
            freePlays: this._model.freePlays,
            jackpots: this._model.jackpots,
            lines: [],
            lobby: null,
            keyboardEnabled: true,
            logoEnabled: true,
            customConfig: {
                customAssets: "default",
                customIndex: -1
            },
            gameRules: () => {},
            extraParams: [],
            type: CommResponseStates.INIT
        }

        let homeURL:string = URLVars.getQueryVariable("homeURL");
        if(homeURL && homeURL.length){
            responseData.lobby = () => {
                location.href = homeURL;
            };
        }

        this._model.balance = responseData.balance.balance;

        return responseData;
    }
}