import { ConcreteTask } from "../../../../utils/task/ConcreteTask";
import { ArrowModel } from "../../ArrowModel";
import { CommSpinResponse } from "../../../../types/CommSpinResponse";
import { CommResponseStates } from "../../../../types/CommResponseStates";
import { ArrowUtils } from "../../ArrowUtils";
import { CommBonusResponse } from "../../../../types/CommBonusResponse";
import { CommErrorResponse } from "../../../../types/CommErrorResponse";

/** @internal */
export class RequestBonus extends ConcreteTask {
    private _model:ArrowModel = ArrowModel.getInstance();

    execute(params: any = null): void {
        let data:any = this.serialize();
        if(!data){
            this.sendCanceled();
            return;
        }

        this._model.sendRequest(data)
            .then(e => {
                let response:any = this._model.parseQuery(<any>e);
                if(response.gp_e != null){
                    let error:CommErrorResponse = {
                        code: response.gp_e,
                        isCritical: true,
                        message: "",
                        userCancelled: false,
                        userConfirmed: false
                    };
                    this.params.responseData = error;

                    this.sendError();
                } else {
                    this.params.response = this.deserialize(response);
                
                    this.sendComplete();
                }
            })
            .fail(e => {
                let error:CommErrorResponse = {
                    code: 0,
                    isCritical: true,
                    message: "",
                    userCancelled: false,
                    userConfirmed: false
                };
                this.params.responseData = error;

                this.sendError();
            });
    }

    private serialize():any {
        let ret:any = {};
        
        ret.gp_gn = this._model.gameId;
        ret.gp_a = this._model.userId;
        ret.gp_ci = this._model.sessionId;
        ret.gp_rm = this._model.playingForReal ? "T" : "F";
        ret.gp_tid = this._model.tournamentId;
        ret.gp_fsbid = this._model.freePlaysId;
        ret.id = this._model.requestId;
        
        // Mandatory
        if(!this.params.params.hasOwnProperty("type")){
            return null;
        }

        ret.t = this.params.params.type;
        for(let prop in this.params.params){
            if(prop != "type"){
                ret[prop] = this.params.params[prop];
            }
        }

        return ret;
    }

    private deserialize(response:any): any {
        this._model.balance = ArrowUtils.toCurrency(response._bal_ic);
        this._model.parseFreePlays(response);
        this._model.parseJackpots(response);

        let ret:CommBonusResponse = {
            type: CommResponseStates.BONUS,
            bonus: ArrowUtils.parseBonus(response),
            getBonus: (name:string) => {}
        };

        return ret;
    }
}