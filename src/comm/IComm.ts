import * as Q from 'q';
import { Signal } from 'signals';
import { CommGameStates } from '../types/CommGameStates';
import { CommInitRequest } from '../types/CommInitRequest';
import { CommGameFreePlaysModes } from '../types/CommGameFreePlaysModes';
import { CommGameJackpot } from '../types/CommGameJackpot';
import { CommSavedResponse } from '../types/CommSavedResponse';
import { CommHistory } from '../types/CommHistory';

/**
 * Main interface for the comm-manager services. 
 * Every communication between the front-end and the back-end should be done through this class.
 */
export interface IComm {
    /**
     * Dispatched when the comm-manager is ready to be used.
     * You should always wait for this signal before calling any method.
     */
    onReady:Signal;

    /**
     * Performs an initialization to the server. Returns a CommInitResponse with the initialization data.
     * @param params 
     * @returns {CommInitResponse}
     */
    init(params:CommInitRequest):Q.Promise<void>;
    /**
     * Requests a SPIN to the server.
     * @param bet Value of a single bet.
     * @param lines Number of lines or ways.
     * @param extraParams Any non-standard extra param that the server might require.
     * @param saveData Info to be saved onto the server.
     * @returns {CommSpinResponse}
     */
    spin(bet:number, lines?:number, extraParams?:any, saveData?:CommSavedResponse[]):Q.Promise<void>;
    /**
     * Requests a BONUS to the server.
     * @param bet Value of a single bet.
     * @param params Any extra params needed for the bonus.
     * @returns {CommBonusResponse}
     */
    bonus(bet:number, params?:any, saveData?:CommSavedResponse[]):Q.Promise<void>;
    /**
     * Requests a GAMBLE to the server.
     * @param bet Value of a single bet.
     * @param params Any extra params needed for the gamble.
     * @returns {CommGambleResponse}
     */
    gamble(bet:number, params?:any, saveData?:CommSavedResponse[]):Q.Promise<void>;
    /**
     * Lets the comm-manager know that the game is starting or ending an AUTOPLAY mode.
     * @param enabled 
     * @param spins Number of spins in the autoplay session.
     * @param betList List of possible bet values.
     * @param stopCallback Method to call if user requested a stop.
     */
    autoplay(enabled:boolean, spins?:number, betList?:Array<number>, stopCallback?:any):Q.Promise<void>;
    /**
     * Closes the current open play (if any), settles the play and saves any aditional data.
     * @param saveData Data to save.
     */
    close(saveData:CommSavedResponse[]):Q.Promise<void>;
    /**
     * Performs a data save onto the server.
     * @param data 
     */
    save(data:CommSavedResponse[]):Q.Promise<void>;
    /**
     * Loads data saved onto the server by id.
     * @param name 
     */
    load(name:string):any;
    /**
     * Gets the current play history (if any) to perform a state recovery.
     * @returns {CommHistory}
     */
    history():CommHistory;
    /**
     * Notifies of an error that has occurred in the game.
     * @param errorCode 
     */
    error(errorCode:number):Q.Promise<void>;
    /**
     * Requests an unloading of data and destroys the instance, this is irreversible.
     */
    destroy():Q.Promise<void>;

    /**
     * Gets if game has been blocked by an outside source.
     */
    isGameBlocked():boolean;
    /**
     * Gets if game is on idle mode.
     */
    isGameIdle():boolean;
    /**
     * Gets if comm-manager is ready to operate.
     */
    isReady():boolean;

    /**
     * Gets the user's balance and free balance.
     */
    getBalance():{balance:number, freeBalance:number};
    /**
     * Gets the current channel.
     */
    getChannel():string;
    /**
     * Gets the current locale.
     */
    getLocale():string;
    /**
     * Gets if general sound is enabled.
     */
    getSoundEnabled():boolean;
    /**
     * Gets if music is enabled.
     */
    getMusicEnabled():boolean;
    /**
     * Gets if sound effects are enabled.
     */
    getSFXEnabled():boolean;
    /**
     * Gets if the game's CONTROL PANEL is enabled.
     */
    getControlPanelEnabled():boolean;
    /**
     * Gets if ABOUT section is enabled.
     */
    getAboutEnabled():boolean;
    /**
     * Gets if PAYTABLE is showing.
     */
    getPaytableEnabled():boolean;
    /**
     * Gets if the game's HELP is showing.
     */
    getHelpEnabled():boolean;
    /**
     * Gets if keyboard is enabled.
     */
    getKeyboardEnabled():boolean;
    /**
     * Gets information regarding the currency.
     * @see {CurrencyConfig}
     */
    getCurrencyConfig():object;
    /**
     * Gets the game's jackpots (if any).
     */
    getJackpots():CommGameJackpot[];
    /**
     * Get the current play id or null if no play has been opened yet.
     */
    getPlayId():number;
    /**
     * Sets the loading progress to inform the server.
     * @param value 
     */
    setProgress(value:number):void;
    /**
     * Sets the game's current state
     * @param state 
     * @see {CommGameStates}
     */
    setGameState(state:CommGameStates):Q.Promise<any>;
    /**
     * Sets the user's balance and free balance.
     * @param balance 
     * @param freeBalance 
     */
    setBalance(balance:number, freeBalance:number):void;
    /**
     * Sets the current bet.
     * @param value 
     */
    setBet(value:number):void;
    /**
     * Sets the winnings for the current play.
     * @param value 
     */
    setWinnings(value:number):void;
    /**
     * Sets if general sound is enabled.
     * @param value 
     */
    setSoundEnabled(value:boolean):void;
    /**
     * Sets if music is enabled.
     * @param value 
     */
    setMusicEnabled(value:boolean):void;
    /**
     * Sets if sound effects are enabled.
     * @param value 
     */
    setSFXEnabled(value:boolean):void;
    /**
     * Sets if the game's CONTROL PANEL is enabled.
     * @param value 
     */
    setControlPanelEnabled(value:boolean):void;
    /**
     * Sets if ABOUT section is showing.
     * @param value 
     */
    setAboutEnabled(value:boolean):void;
    /**
     * Sets if PAYTABLE is showing.
     * @param value 
     */
    setPaytableEnabled(value:boolean):void;
    /**
     * Sets if game's HELP is showing.
     * @param value 
     */
    setHelpEnabled(value:boolean):void;
    /**
     * Sets if the game has entered a FREE PLAYS mode.
     * @param value 
     */
    setFreePlaysMode(value:CommGameFreePlaysModes):void;
}