import { CommFactory } from './CommFactory';
import { IComm } from './comm/IComm';
import { CommServers } from './CommServers';

/**
 * Exposed entry point for the comm-manager. Returns an IComm instance depending on the CommServer specified.
 */
export class CommManager {
    public static getComm(server:CommServers):IComm {
        return CommFactory.getComm(server);
    }
}