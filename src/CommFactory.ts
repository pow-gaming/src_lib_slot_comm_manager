import { ArrowComm } from "./comm/arrow/ArrowComm";
import { DummyComm } from "./comm/dummy/DummyComm";
import { LegaComm } from "./comm/lega/LegaComm";
import { IComm } from "./comm/IComm";
import { CommServers } from "./CommServers";

/**
 * Creates a new IComm based on the CommServer specified
 */
/** @internal */
export class CommFactory {
    constructor(){

    }

    public static getComm(server:CommServers):IComm {
        let ret:IComm;

        switch(server){
            case CommServers.LEGA:
                ret = new LegaComm();
            break;
            case CommServers.ARROW:
                ret = new ArrowComm();
            break;
            case CommServers.DEFAULT:
                ret = new DummyComm();
            break;
        }

        return ret;
    }
}