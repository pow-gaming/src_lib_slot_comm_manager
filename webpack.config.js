const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const version = JSON.stringify(require('./package.json').version);
const TypescriptDeclarationPlugin = require('@vibragaming/typescript-declaration-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    context: process.cwd(),
    externals: [],
    entry: {
        'index': path.join(__dirname, '/src/index.ts'),
        'index.min': path.join(__dirname, '/src/index.ts')
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].js',
        libraryTarget: 'commonjs'
    },
    devtool: false,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    performance: { 
        hints: false 
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    optimization: {
        minimizer: [new UglifyJsPlugin({
            include: /.min.js$/,
            sourceMap: false
        })],
    },
    plugins: [             
        new webpack.DefinePlugin({
            VERSION: version
        }),
        new CleanWebpackPlugin(),
        new TypescriptDeclarationPlugin({
            out: './index.d.ts'
        })
    ]
};